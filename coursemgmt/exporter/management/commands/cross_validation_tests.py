import requests
from crawler.feature_extraction.lda_features_extractor import LdaFeaturesExtractor
from crawler.feature_extraction.lsa import LsaFeaturesExtractor
from crawler.feature_extraction.tf_idf import TfIdf
from django.core.management import BaseCommand
from exporter.core import ExportController


class Command(BaseCommand):
    def handle(self, **options):
        """
        Export the crawler database to meteor
        :param options:
        :return:
        """
        run_hybrid_test()
        # run_cf_test_parameters()
        # run_cf_test_rating_count()
        # run_tf_idf_test()
        # run_lda_test()
        # run_lsa_test()


def run_cf_test_parameters():
    output_file = open('cf_evaluation_results.txt', 'w', 0)
    request_parameters = {
        'method': 'item-item'
        # 'method': 'user-user'
    }
    for vector_similarity in ['cosine', 'pearson']:
        request_parameters['vectorSimilarity'] = vector_similarity
        for neighborhood_size in [5, 10, 20]:
            request_parameters['neighbourhoodSize'] = neighborhood_size
            for min_neighbor_count in [1, 5]:
                request_parameters["minNeighborCount"] = min_neighbor_count
                r = requests.get("http://localhost:8090/evaluate/cross-validation/cf",
                                 params=request_parameters)
                if r.status_code != 200:
                    print('error - status code ' + str(r.status_code))
                else:
                    json = r.json()
                    output_file.write('Similarity:' + vector_similarity + '\n')
                    output_file.write("Neighborhood Size: " + str(neighborhood_size) + '\n')
                    output_file.write('Min Neighbor Count:' + str(min_neighbor_count) + '\n')
                    for entry in json:
                        line = str(entry['name']) + ': ' + str(entry['result']) + '\n'
                        output_file.write(line)
                    output_file.write('\n\n')


def run_cf_test_rating_count():
    output_file = open('cf_rating_counts.txt', 'w', 0)
    request_parameters = {
        'studyCourse': 'economics'
    }
    for rating_count in parameter_range(3200, 11988, 100):
        request_parameters['ratingCount'] = rating_count
        r = requests.get("http://localhost:8090/evaluate/cross-validation/cb",
                         params=request_parameters)
        if r.status_code != 200:
            print('error - status code ' + str(r.status_code))
        else:
            json = r.json()
            output_file.write('Rating Count:' + str(rating_count) + '\n')
            for entry in json:
                line = str(entry['name']) + ': ' + str(entry['result']) + '\n'
                output_file.write(line)
            output_file.write('\n\n')


def run_hybrid_test():
    output_file = open('hybrid_results.txt', 'w', 0)
    for cf_percentage in parameter_range(0.01, 1.0, 0.01):
        r = requests.get("http://localhost:8090/evaluate/cross-validation/hybrid",
                         params={'cfPercentage': cf_percentage})
        if r.status_code != 200:
            print('error - status code ' + str(r.status_code))
        else:
            json = r.json()
            line = str(cf_percentage) + ' CF\n'
            for entry in json:
                line += str(entry['name']) + ' ' + str(entry['result']) + '\n'
            print(line)
            output_file.write(line + '\n\n')


def run_tf_idf_test():
    output_file = open('tfidf_evaluation.txt', 'w', 0)
    for number_of_keywords in [5, 10, 15, 20, 10000]:  # [10, 20, 40, 80, 150]:
        TfIdf(max_keywords=number_of_keywords).calculate_module_features()
        ExportController('dev').export_to_recommender()
        request_parameters = {}
        for vector_similarity in ['cosine', 'pearson']:
            request_parameters['vectorSimilarity'] = vector_similarity
            r = requests.get('http://localhost:8090/evaluate/cross-validation/cb',
                             params=request_parameters)
            if r.status_code != 200:
                print('error - status code ' + str(r.status_code))
            else:
                json = r.json()
                line = 'similarity: ' + str(request_parameters['vectorSimilarity']) + ', '
                line += 'number of keywords: ' + str(number_of_keywords) + '\n'
                for entry in json:
                    line += str(entry['name']) + ': ' + str(entry['result']) + '\n'
                print(line)
                output_file.write(line + '\n\n')


def run_lda_test():
    output_file = open('lda_evaluation.txt', 'w', 0)
    for number_of_topics in [300, 350]:
        for words_per_topic in [2, 3]:
            for min_df in [0.005, 0.01]:
                for max_df in [.1]:
                    if min_df < max_df:
                        LdaFeaturesExtractor(number_of_topics, words_per_topic, min_df,
                                             max_df).extract_lda_features()
                        ExportController('dev').export_to_recommender()
                        request_parameters = {}
                        for vector_similarity in ['cosine']:
                            request_parameters['vectorSimilarity'] = vector_similarity
                            r = requests.get('http://localhost:8090/evaluate/cross-validation/cb',
                                             params=request_parameters)
                            if r.status_code != 200:
                                print('error - status code ' + str(r.status_code))
                            else:
                                json = r.json()
                                line = 'similarity: ' + str(request_parameters['vectorSimilarity']) + ', '
                                line += 'number of topics: ' + str(number_of_topics) + ', '
                                line += 'words per topic: ' + str(words_per_topic) + ', '
                                line += 'min df: ' + str(min_df) + ', '
                                line += 'max df: ' + str(max_df) + '\n'
                                for entry in json:
                                    line += str(entry['name']) + ': ' + str(entry['result']) + '\n'
                                print(line)
                                output_file.write(line + '\n\n')


def run_lsa_test():
    output_file = open('lsa_evaluation.txt', 'w', 0)
    for number_of_topics in [900]:
        for number_of_features in [10, 20, 25]:
            LsaFeaturesExtractor(number_of_topics=number_of_topics, number_of_features=number_of_features)
            ExportController('dev').export_to_recommender()
            request_parameters = {}
            for vector_similarity in ['cosine']:
                request_parameters['vectorSimilarity'] = vector_similarity
                r = requests.get('http://localhost:8090/evaluate/cross-validation/cb',
                                 params=request_parameters)
                if r.status_code != 200:
                    print('error - status code ' + str(r.status_code))
                else:
                    json = r.json()
                    line = 'similarity: ' + str(request_parameters['vectorSimilarity']) + ', '
                    line += 'number of topics: ' + str(number_of_topics) + ', '
                    line += 'words per topic: ' + str(number_of_features) + '\n'
                    for entry in json:
                        line += str(entry['name']) + ': ' + str(entry['result']) + '\n'
                    print(line)
                    output_file.write(line + '\n\n')


def parameter_range(start, end, step):
    while start <= end:
        yield start
        start += step
