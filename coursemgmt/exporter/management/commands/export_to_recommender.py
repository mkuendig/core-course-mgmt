"""
Export the crawler database to meteor
"""
from django.core.management import BaseCommand, CommandError
from exporter.core import ExportController


class Command(BaseCommand):
    """
    Export modules and weighted features to recommender
    """

    def handle(self, branch, **options):
        """
        Export the crawler database to meteor
        :param branch: either "dev", "accpt", or "prod"
        :param options:
        :return:
        """
        ExportController(branch).export_to_recommender()
