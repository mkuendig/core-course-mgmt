"""
Export the crawler database to meteor
"""
from django.core.management import BaseCommand
from exporter.core import ExportController


class Command(BaseCommand):
    """
    Crawl the whole course catalog for a given semester
    """
    help = 'Export the crawler database to meteor'

    def handle(self, branch, **options):
        """
        Export the crawler database to meteor
        :param branch: either "dev", "accpt", or "prod"
        :param options:
        :return:
        """
        ExportController(branch).export_to_meteor()
