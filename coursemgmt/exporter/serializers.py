from crawler.models import Module, Semester, Lecture, Category, Person, Room, LectureScheduleItem, ModuleOrganizational, \
    CustomCategory, ModuleFeatureRelation
from rest_framework import serializers


class CustomCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomCategory
        fields = ('title', 'slug', 'module_type', 'parents')
        depth = 10


class ModuleCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomCategory
        fields = ('title', 'slug', 'module_type')


class CategorySerializer(serializers.ModelSerializer):
    fields = ModuleCategorySerializer(source='custom_categories')

    class Meta:
        model = Category
        fields = ('fields', 'parents')


"""
Workaround to make recursive category serialization possible
http://stackoverflow.com/questions/13376894/django-rest-framework-nested-self-referential-objects
"""
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()
CategorySerializer.base_fields['parents'] = CategorySerializer()


class PersonSerializer(serializers.ModelSerializer):
    uzhId = serializers.CharField(source='uzh_id')

    class Meta:
        model = Person
        fields = ('uzhId', 'firstname', 'lastname', 'title', 'position')


class RoomSerializer(serializers.ModelSerializer):
    uzhId = serializers.CharField(source='uzh_id')

    class Meta:
        model = Room
        fields = ('uzhId', 'name', 'street', 'address')


class ScheduleItemSerializer(serializers.ModelSerializer):
    rooms = RoomSerializer(many=True)
    lecturers = PersonSerializer(many=True)
    startDatetime = serializers.RelatedField(source='start_datetime')
    endDatetime = serializers.RelatedField(source='end_datetime')

    class Meta:
        model = LectureScheduleItem
        fields = ('startDatetime', 'endDatetime', 'lecturers', 'rooms')


class LectureSerializer(serializers.ModelSerializer):
    uzhId = serializers.CharField(source='uzh_id')
    scheduleItems = ScheduleItemSerializer(many=True, source='lecture_schedule_item')
    lectureCategory = serializers.CharField(source='category')

    class Meta:
        model = Lecture
        fields = ('uzhId', 'title', 'lectureCategory', 'scheduleItems')


class SemesterSerializer(serializers.ModelSerializer):
    semesterStart = serializers.CharField(source='semester_start')
    semesterEnd = serializers.CharField(source='semester_end')
    crawlDate = serializers.CharField(source='crawl_date')

    class Meta:
        model = Semester
        fields = ('title', 'semesterStart', 'semesterEnd', 'crawlDate')


class OrganizationalUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuleOrganizational
        fields = ('title', 'url')


class FeatureRelationSerializer(serializers.ModelSerializer):
    moduleId = serializers.RelatedField(source='module.uzh_code')
    featureName = serializers.RelatedField(source='feature')

    class Meta:
        model = ModuleFeatureRelation
        fields = ('moduleId', 'featureName', 'weight')


class BaseModuleSerializer(serializers.ModelSerializer):
    semester = SemesterSerializer()
    categories = CategorySerializer(many=True)
    description = serializers.RelatedField(source='description')
    lectures = LectureSerializer(many=True)
    moduleId = serializers.CharField(source='uzh_code')
    bookDeadline = serializers.RelatedField(source='booking_deadline')
    cancelDeadline = serializers.RelatedField(source='cancellation_deadline')
    personInCharge = PersonSerializer(many=True, source='persons_in_charge')
    organizationalUnits = OrganizationalUnitSerializer(many=True, source='organizational_units')
    language = serializers.RelatedField(source='language')
    uzhUrl = serializers.RelatedField(source='uzh_url')


class MeteorModuleSerializer(BaseModuleSerializer):
    class Meta:
        model = Module
        fields = (
            'moduleId', 'uzhUrl', 'semester', 'title', 'categories', 'description', 'ects', 'language', 'bookDeadline',
            'cancelDeadline', 'personInCharge', 'organizationalUnits', 'lectures'
        )


class RecommenderModuleSerializer(BaseModuleSerializer):
    class Meta:
        model = Module
        fields = (
            'moduleId', 'title', 'categories', 'personInCharge', 'ects', 'language'
        )
