"""
Controller class to export the database to meteor
"""

import ddp
import os
import requests
from crawler.models import Module, Person, CustomCategory, Room, ModuleFeatureRelation
from django.conf import settings
from rest_framework.renderers import JSONRenderer
from serializers import PersonSerializer, CustomCategorySerializer, RoomSerializer, \
    FeatureRelationSerializer, MeteorModuleSerializer, RecommenderModuleSerializer

METEOR_DEV_EXPORT_URL = 'ws://127.0.0.1:3000/websocket'
METEOR_ACCPT_EXPORT_URL = 'ws://accpt.core.ifi.uzh.ch:3000/websocket'
METEOR_PROD_EXPORT_URL = 'ws://core.ifi.uzh.ch:3000/websocket'

RECOMMENDER_DEV_EXPORT_URL = 'http://127.0.0.1:8090/export/coursemgmt'
RECOMMENDER_ACCPT_EXPORT_URL = 'http://accpt.core.ifi.uzh.ch:8090/export/coursemgmt'
RECOMMENDER_PROD_EXPORT_URL = 'http://core.ifi.uzh.ch:8090/export/coursemgmt'


class ExportController(object):
    """
    Controller class to export the database to meteor
    """
    client = None
    recommender_url = ""

    def __init__(self, branch):
        # Create a client, passing the URL of the server.
        if branch == "dev":
            self.client = ddp.ConcurrentDDPClient(METEOR_DEV_EXPORT_URL)
            self.recommender_url = RECOMMENDER_DEV_EXPORT_URL
        elif branch == "accpt":
            self.client = ddp.ConcurrentDDPClient(METEOR_ACCPT_EXPORT_URL)
            self.recommender_url = RECOMMENDER_ACCPT_EXPORT_URL
        elif branch == "prod":
            self.client = ddp.ConcurrentDDPClient(METEOR_PROD_EXPORT_URL)
            self.recommender_url = RECOMMENDER_PROD_EXPORT_URL

    def export_to_recommender(self):
        json_renderer = JSONRenderer()
        if not os.path.exists('modules_recommender.json'):
            modules_json = self._create_recommender_modules_json(json_renderer)
            self._save_as_file(modules_json, 'modules_recommender.json')
        else:
            with open('modules_recommender.json') as data_file:
                modules_json = data_file.read()
        self._call_recommender(modules_json, '/modules')

        # if not os.path.exists('features.json'):
        features_json = self._create_features_json(json_renderer)
        self._save_as_file(modules_json, 'features.json')
        # else:
        #     with open('features.json') as data_file:
        #         features_json = data_file.read()
        self._call_recommender(features_json, '/features')

    def export_to_meteor(self):
        """
        Export the database to meteor
        :return:
        """

        json_renderer = CustomRenderer()
        # Once started, the client will maintain a connection to the server.
        self.client.start()

        persons_json = self._create_persons_json(json_renderer)
        self._call_meteor(persons_json)

        categories_json = self._create_categories_json(json_renderer)
        self._call_meteor(categories_json)

        rooms_json = self._create_rooms_json(json_renderer)
        self._call_meteor(rooms_json)

        if not os.path.exists('modules_meteor.json'):
            modules_json = self._create_meteor_modules_json(json_renderer)
            self._save_as_file(modules_json, 'modules_meteor.json')
        else:
            with open('modules_meteor.json') as data_file:
                modules_json = data_file.read()
        self._call_meteor(modules_json)

        # Ask the client to stop and wait for it to do so.
        self.client.stop()
        self.client.join()

    @staticmethod
    def _create_persons_json(json_renderer):
        print('Start serializing persons')
        person_serializer = PersonSerializer(Person.objects.all().order_by('uzh_id', '-semester').distinct('uzh_id'))
        print('Start render persons as json')
        return json_renderer.render(person_serializer.data, 'persons')

    @staticmethod
    def _create_categories_json(json_renderer):
        print('Start serializing categories')
        category_serializer = CustomCategorySerializer(CustomCategory.objects.all())
        print('Start render categories as json')
        return json_renderer.render(category_serializer.data, 'categories')

    @staticmethod
    def _create_rooms_json(json_renderer):
        print('Start serializing rooms')
        room_serializer = RoomSerializer(Room.objects.all().order_by('uzh_id', '-semester').distinct('uzh_id'))
        print('Start render rooms as json')
        return json_renderer.render(room_serializer.data, 'rooms')

    @staticmethod
    def _create_meteor_modules_json(json_renderer):
        print('Start serializing modules')
        module_serializer = MeteorModuleSerializer(
                Module.objects.filter(lectures__category__is_active=True).order_by('uzh_code', '-semester').distinct(
                        'uzh_code'))
        print('Start render modules as json')
        return json_renderer.render(module_serializer.data, 'modules')

    @staticmethod
    def _create_recommender_modules_json(json_renderer):
        print('Start serializing modules')
        module_serializer = RecommenderModuleSerializer(
                Module.objects.filter().order_by('uzh_code', '-semester').distinct(
                        'uzh_code'))
        print('Start render modules as json')
        return json_renderer.render(module_serializer.data, 'modules')

    @staticmethod
    def _create_features_json(json_renderer):
        print('Start serializing features')
        feature_serializer = FeatureRelationSerializer(ModuleFeatureRelation.objects.all())
        print('Start render features as json')
        return json_renderer.render(feature_serializer.data)

    def _call_meteor(self, export_json):
        print('Start sending data to meteor')
        # The method is executed asynchronously.
        future = self.client.call('receiveDataFromCrawler', export_json)

        # Block until the result message is received.
        result_message = future.get()

        # Check if an error occurred else print the result.
        if result_message.has_error():
            print result_message.error
        else:
            print result_message.result

    def _call_recommender(self, export_json, url_path):
        print('Start sending json to recommender')
        requests.post(self.recommender_url + url_path, data=export_json,
                      headers={'Content-type': 'application/json'})

    @staticmethod
    def _save_as_file(json, file_name):
        json_file = open(file_name, 'w')
        json_file.write(json)
        json_file.close()


class CustomRenderer(JSONRenderer):
    def render(self, data, root, accepted_media_type=None, renderer_context=None):
        data = {root: data, 'password': settings.METEOR_EXPORT_PASSWORD}
        return super(CustomRenderer, self).render(data, accepted_media_type, renderer_context)
