from huey import crontab
from huey.djhuey import periodic_task
from django_fabfile.utils.git import get_current_git_branch
from fabric.api import local


@periodic_task(crontab(hour='*/8'))
def backup():
    """
    Calls the fab backup command
    """
    branch = get_current_git_branch()
    local('cd ../ && fab backup:{branch},is_on_server=True,media=True'.format(branch=branch))
    return 'Backup successful'
