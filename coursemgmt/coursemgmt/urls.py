from django.conf.urls import *  # pylint: disable=W0401,W0614
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

urlpatterns = i18n_patterns('',
                            (r'^grappelli/', include('grappelli.urls')),  # grappelli URLS
                            url(r'^admin/', include(admin.site.urls)),
                            url(r'^$', include(admin.site.urls)) )

if settings.DEBUG:
    urlpatterns = patterns('',
                           url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                               {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
                           url(r'', include('django.contrib.staticfiles.urls')),
                           ) + urlpatterns

# add a few semesters on startup
import crawler.startup as startup
startup.add_semesters()
