"""
Production settings and globals.
"""

from base import *  # pylint: disable=W0401,W0614

########## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'coursemgmt_prod',
        'USER': 'coursemgmt_db_user',
        'PASSWORD': 'NhwM6Acf!IDz',
        'HOST': '',
        'PORT': '',
    }
}
########## END DATABASE CONFIGURATION

########## HOST CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/releases/1.5/#allowed-hosts-required-in-production
########## END HOST CONFIGURATION

########## RAVEN / SENTRY CONFIGURATION
INSTALLED_APPS += (
    'raven.contrib.django.raven_compat',
)

MIDDLEWARE_CLASSES += (
    'raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware',
    'raven.contrib.django.raven_compat.middleware.Sentry404CatchMiddleware',
)

RAVEN_CONFIG = {
    'dsn': 'http://c8e90210181d44c39211acc7e3b595f0:d7563dd5f63247f2b784ddb28dbb55fb@sentry.philippundhee.ch/2',
    'site': 'coursemgmt',
    'name': 'Prod',
    'auto_log_stacks': True
}
########## END RAVEN / SENTRY CONFIGURATION

########## LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': ' %(asctime)s %(levelname)s %(name)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'filters': ['require_debug_false'],
            'formatter': 'verbose'
        },
        'sentry': {
            'level': 'INFO',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['sentry', 'mail_admins'],
            'propagate': True,
            'level': 'ERROR',
        },
        'huey.consumer': {
            'handlers': ['sentry', 'mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'coursemgmt': {
            'handlers': ['sentry'],
            'propagate': True,
            'level': 'INFO',
        },
    }
}

########## END LOGGING CONFIGURATION

########## HUEY CONFIGURATION
INSTALLED_APPS += (
    'huey.djhuey',
)

HUEY = {
    'backend': 'huey.backends.redis_backend',
    'name': 'coursemgmt_prod',
    'connection': {'host': '', 'port': '', 'password': ''},
    'always_eager': False,
    'consumer_options': {'workers': 1},
}
########## END HUEY CONFIGURATION

########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'coursemgmt_cache_prod',
    }
}

#Does not cache when logged in
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True
CACHE_MIDDLEWARE_SECONDS = 3600
CACHE_MIDDLEWARE_KEY_PREFIX = 'coursemgmt'
########## END CACHE CONFIGURATION
