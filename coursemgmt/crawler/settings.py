# coding=utf-8
"""
Basic Scrapy Settings

All the other settings are documented here:
http://doc.scrapy.org/en/latest/topics/settings.html
"""

# The name of the bot implemented by this Scrapy project
BOT_NAME = 'uzh_crawler'

TAXONOMY_SETTINGS = {
    'name': 'taxonomy',  # Name of the spider
    'start_url': 'http://www.vorlesungen.uzh.ch/{semester}/lehrangebot/fak-50000003.html',  # Start urls for the spider
    'allowed_domains': ['vorlesungen.uzh.ch'],  # restrict to subdomains only
    'xpath_navigation': u'//*[@id="secnav"]//a[@class="active"]/..',  # xpath identifier for the left hand navigation
    'xpath_title': u'//*[@id="secnav"]//a[@class="active"]/text()',  # xpath identifier to extract title
    'module_prefix': 'sm-.*',  # regex identifier to identify a module
    'root_teaching': 'lehrangebot/',  # url element before the hierarchy of categories starts
    'category_modules_url_id': 'module',  # url suffix to identify lecture details page
    'category_details_url_id': 'details',
    'xpaths_to_extract': {
        'study_degree': u'//table[@class="ornate"]//th[text()="Abschluss:"]/../td//text()',
        'study_content': u'//table[@class="ornate"]//th[contains(text(), "Studienziele")]/../td//text()',
        'total_ects': u'//table[@class="ornate"]//th[contains(text(), "ECTS")]/../td//text()',
        'major_minor_regulation': u'//table[@class="ornate"]//th[contains(text(), "HF-/NF-Kombinationen:")]/../td//text()',
        'precognition': u'//table[@class="ornate"]//th[text()="Besondere Vorkenntnisse:"]/../td//text()',
        'field_of_study': u'//table[@class="ornate"]//th[contains(text(), "Studienrichtung")]/../td//text()'
    }
}

MODULE_SETTINGS = {
    'name': 'module',  # Name of the spider
    'allowed_domains': ['vorlesungen.uzh.ch'],  # restrict to subdomains only
    'xpath_pagination': u'//div[@class="pagination"][1]//a[text() = "»"]',  # xpath identifier for pagination
    'xpath_modules_list': u'//table[@class="ornate vvzDetails"]/tbody',  # xpath identifier for the module overview list
    'module_prefix': 'sm-.*',  # regex identifier to identify a module
    'persons_in_charge': u'//table[@class="ornate"]//th[text()="Modulverantwortliche/r:"]/../td/a',
    'organizational_units': u'//table[@class="ornate"]//th[text()="Anbietende Organisationseinheit/en:"]/../td/a',
    'xpaths_to_extract': {
        'title': u'//h1/span[@class="document-title-name"]/text()',  # xpath identifier to extract title,
        'uzh_code': u'//table[@class="ornate"]//th[text()="Modulkürzel:"]/../td//text()',
        'ects': u'//table[@class="ornate"]//th[text()="ECTS-Punkte:"]/../td//text()',
        'frequency': u'//table[@class="ornate"]//th[text()="Dauer und Angebotsmuster:"]/../td//text()',
        'description': u'//table[@class="ornate"]//th[text()="Allgemeine Beschreibung:"]/../td//text()',
        'precognition': u'//table[@class="ornate"]//th[text()="Vorkenntnisse:"]/../td//text()',
        'requirements': u'//table[@class="ornate"]//th[text()="Voraussetzungen:"]/../td//text()',
        'teaching_materials': u'//table[@class="ornate"]//th[text()="Unterrichtsmaterialien:"]/../td//text()',
        'learning_target': u'//table[@class="ornate"]//th[text()="Lernziele:"]/../td//text()',
        'target_audience': u'//table[@class="ornate"]//th[text()="Zielgruppen:"]/../td//text()',
        'achievement_test': u'//table[@class="ornate"]//th[text()="Leistungsüberprüfung:"]/../td//text()',
        'grading_scale': u'//table[@class="ornate"]//th[text()="Notenskala:"]/../td//text()',
        'repeatability': u'//table[@class="ornate"]//th[text()="Repetierbarkeit:"]/../td//text()',
        'language': u'//table[@class="ornate"]//th[text()="Sprache:"]/../td//text()',
        'further_information': u'//table[@class="ornate"]//th[text()="Weitere Informationen:"]/../td//text()',
        'booking_deadline': u'//table[@class="ornate"]//th[text()="Buchungsfrist:"]/../td//text()',
        'cancellation_deadline': u'//table[@class="ornate"]//th[text()="Stornierungsfrist:"]/../td//text()'
    }
}

LECTURE_SETTINGS = {
    'name': 'lecture',  # Name of the spider
    'allowed_domains': ['vorlesungen.uzh.ch'],  # restrict to subdomains only
    'xpath_lecture_details': u'//table[@class="ornate vvzDetails"]/tbody/tr/td[2]',
    # xpath identifier for lecture details page
    'xpath_lecturer_schedules': u'//table[@class="ornate vvzDetails"]/tbody/tr',
    'rel_xpath_schedule_date': u'./td[1]/text()',  # relative (from starting from tr tag) xpath to schedule date
    'rel_xpath_schedule_time': u'./td[2]/text()',  # relative (from starting from tr tag) xpath to schedule time
    'rel_xpath_rooms': u'./td[3]/a',
    'rel_xpath_lecturers': u'./td[4]/a',
    'lecture_details_url_id': 'details',  # url suffix to identify lecture details page
    'lecture_schedule_url_id': 'termine',  # url suffix to identify lecture schedule page
    'xpaths_to_extract': {
        'title': u'//h1/span[@class="document-title-name"]/text()',  # xpath identifier to extract title
        'uzh_lecture_id': u'//table[@class="ornate"]//th[text()="Lehrveranstaltungsnummer:"]/../td//text()',
        'uzh_lecture_code': u'//table[@class="ornate"]//th[text()="Lehrveranstaltungskürzel:"]/../td//text()',
        'category': u'//table[@class="ornate"]//th[text()="Kategorie:"]/../td//text()',
        'content': u'//table[@class="ornate"]//th[text()="Lehrveranstaltungsinhalt:"]/../td//text()',
        'teaching_materials': u'//table[@class="ornate"]//th[text()="Unterrichtsmaterialien:"]/../td//text()',
    }
}

PERSON_SETTINGS = {
    'xpaths_to_extract': {
        'firstname': u'//table[@class="ornate"]//th[text()="Vorname:"]/../td//text()',
        'lastname': u'//table[@class="ornate"]//th[text()="Nachname:"]/../td//text()',
        'title': u'//table[@class="ornate"]//th[text()="Titel:"]/../td//text()',  # extract title of person (e.g. Prof.)
        'position': u'//table[@class="ornate"]//th[text()="Funktion:"]/../td//text()',
    }
}

ROOM_SETTINGS = {
    'xpaths_to_extract': {
        'name': u'//table[@class="ornate"]//th[text()="Raum:"]/../td//text()[1]',  # extract room name,
        'street': u'//table[@class="ornate"]//th[text()="Adresse:"]/../td//text()[1]',  # extract street name & nr,
        'address': u'//table[@class="ornate"]//th[text()="Adresse:"]/../td//text()[2]',  # extract postal code & city
    }
}

LECTURE_PIPELINE_SETTINGS = {
    'date_format': '%d.%m.%Y %H:%M'
}
