"""
The core controller of the crawling process
"""
from multiprocessing import Process

from crawler.models import Category
from crawler.models import Module
from crawler.models import Semester
from crawler.spiders.lecture_spider import LectureSpider
from scrapy import log, signals
from scrapy.crawler import Crawler
from scrapy.log import INFO
from scrapy.utils.project import get_project_settings
from spiders.module_spider import ModuleSpider
from spiders.taxonomy_spider import TaxonomySpider
from twisted.internet import reactor

TAXONOMY_PIPELINE = 'crawler.pipelines.taxonomy_pipeline.TaxonomyPipeline'
MODULE_PIPELINE = 'crawler.pipelines.module_pipeline.ModulePipeline'
LECTURE_PIPELINE = 'crawler.pipelines.lecture_pipeline.LecturePipeline'
PERSON_PIPELINE = 'crawler.pipelines.person_pipeline.PersonPipeline'
ROOM_PIPELINE = 'crawler.pipelines.room_pipeline.RoomPipeline'
ITEM_PIPELINES = 'ITEM_PIPELINES'


class CrawlerController(object):
    """
    Controller for all crawls. Allows you to start all crawl processes.
    """

    def __init__(self, semester_name=""):
        """

        :param semester_name: semester to be crawled
        """
        self.semester_name = semester_name.upper()

    def crawl_semester(self):
        """
        Crawl the entire course catalog for a given semester
        No preconditions.
        """
        self.crawl_taxonomy()
        self.crawl_modules()
        self.crawl_lectures()

    def crawl_taxonomy(self):
        """
        Crawl method starting the crawl process of the taxonomy for a semester.
        No preconditions.
        """
        spider = TaxonomySpider(semester_name=self.semester_name)
        settings = self._get_scrapy_settings(TAXONOMY_PIPELINE)
        crawler = MultiprocessingCrawler(settings)
        crawler.crawl(spider)

    def crawl_modules(self):
        """
        Core crawl method starting the crawl process of an entire semester
        Taxonomy has to be crawled.
        """
        semester = Semester.objects.get(title=self.semester_name)
        categories = Category.objects.filter(semester=semester).order_by('-level')
        for category in categories:
            if u'module' in category.uzh_url:
                self._crawl_module(category)

    def _crawl_module(self, category):
        spider = ModuleSpider(category=category)
        settings = self._get_scrapy_settings(MODULE_PIPELINE)
        crawler = MultiprocessingCrawler(settings)
        crawler.crawl(spider)

    def crawl_lectures(self):
        """
        Core crawl method starting the crawl process of an entire semester
        Modules have to be crawled.
        """
        semester = Semester.objects.get(title=self.semester_name)
        modules = Module.objects.filter(semester=semester)
        for module in modules:
            spider = LectureSpider(module=module)
            settings = self._get_scrapy_settings(LECTURE_PIPELINE)
            crawler = MultiprocessingCrawler(settings)
            crawler.crawl(spider)

    @staticmethod
    def _get_scrapy_settings(pipeline):
        settings = get_project_settings()
        settings.overrides[ITEM_PIPELINES] = {
            pipeline: 1,
        }
        return settings


class MultiprocessingCrawler(object):
    """
    Helper class to achieve multiple crawl iteration. Otherwise the reactor cannot be restarted
    """

    def __init__(self, settings):
        # set all settings and configure the crawler
        self.crawler = Crawler(settings)
        self.crawler.signals.connect(reactor.stop, signal=signals.spider_closed)
        self.crawler.configure()

    def _crawl(self, spider):
        self.crawler.crawl(spider)
        self.crawler.start()
        log.start(loglevel=INFO)
        reactor.run()
        # self.crawler.stop()

    def crawl(self, spider):
        """
        Start the crawl process for the given spider
        :param spider: spider to be crawled
        """
        process = Process(target=self._crawl, args=[spider])
        process.start()
        process.join()
