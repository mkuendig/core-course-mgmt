��    [      �     �      �     �     �     �     �       
        #     ,     H     P  
   c     n  	   }  
   �     �     �     �     �     �     �     �     �     	  
   	     	     #	  	   2	     <	  	   P	     Z	     f	     o	     	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     &
     6
     F
     W
     j
     {
     �
     �
     �
     �
     �
     �
               -     D     [     p     |     �     �     �     �     �     �     �     �     �     �     �     �  	   �                    *     :     M  
   S     ^     l     p     y     �     �     �     �  k  �          2     :     I     W  
   i  	   t     ~     �     �     �     �  	   �  
   �     �  '   �     �       	   !     +  
   C     N     S  
   _     j     v     �     �     �     �     �  	   �     �     �     �     �               "     (     =     K     \  	   b     l     r     z     �  
   �     �     �     �     �     �               "     7     N     ^     n     z     �     �     �     �     �     �     �     �     �     �     �                          &     @     L     c     i  2   u     �     �     �     �     �     �  
   �     5   I   )      :      $               K   /   C   4   [   '   P      J   ;       <   Z           D   U      H       8       X   #      (       	       -   E   W   R         1              9       *                           +   M      >      .      O          S   L          ?   Y   %   &                       @           V      A       T   N   !   7   3           0      2   B       F   Q              6       ,   "                         
       =                  G       achievement_test address administration booking_deadline cancellation_deadline categories category compulsory_elective_modules content course_information crawl_date crawl_end_date crawl_job crawl_jobs crawl_start_date create_or_update custom_categories custom_category degree description elective_modules end_datetime etcs export_job export_jobs field_of_study firstname further_information is_active is_finished lastname learning_target lecture lecture_categories lecture_category lecture_schedule_item lecture_schedule_items lectures level major_minor_regulations mandatory_modules mixed_modules module module_category module_deadline module_deadlines module_frequencies module_frequency module_grading_scale module_grading_scales module_keyword module_keyword_relation module_keyword_relations module_keywords module_language module_languages module_organizational module_organizationals module_repeatabilities module_repeatability module_type modules name person person_in_charge persons position precognition requirements room rooms semester semester_end semester_start semesters start_datetime street study_content target_audience teaching_materials title total_etcs update_parent url uzh_code uzh_id uzh_lecture_code uzh_lecture_id uzh_url weight Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-10-01 10:20+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Leistungsüberprüfung Adresse Administration Buchungsfrist Stornierungsfrist Kategorien Kategorie Wahlpflichtmodule Inhalte Kursinformationen Crawl Datum Ende Crawl Job Crawl Jobs Start Erstellt oder aktualisiert durch Skript Eigene Kategorien Eigene Kategorie Abschluss Allgemeine Beschreibung Wahlmodule Ende ECTS-Punkte Export Job Export Jobs Studienrichtung Vorname Weitere Informationen Aktiv Fertig Nachname Lernziele Lehrveranstaltung Vorlesungskategorien Vorlesungskategorie Termin Termine Lehrveranstaltungen Level HF-/NF-Kombinationen Pflichtmodule Gemischte Module Modul Kategorie Frist Fristen Dauer und Angebotsmuster Dauer und Angebotsmuster Notenskala Notenskalen Schlüsselwort Modul Schlüsselwort Modul Schlüsselwörter Schlüsselwörter Sprache Sprachen Organisationseinheit Organisationseinheiten Repetierbarkeit Repetierbarkeit Modul Typus Module Name Person Modulverantwortliche/r Personen Funktion Besondere Vorkenntnisse Voraussetzungen Raum Räume Semester Semesterende Semesterstart Semester Beginn Strasse Studienziele und -inhalte Zielgruppen Unterrichtsmaterialien Titel ECTS-Punkte Übergeordnete Kategorie durch Skript aktualisiert URL Modulkürzel UZH ID Lehrveranstaltungskürzel Lehrveranstaltungsnummer UZH Url Gewichtung 