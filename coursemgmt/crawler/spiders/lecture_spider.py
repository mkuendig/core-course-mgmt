"""
Spiders all modules from a given category
"""
from crawler.items import LectureItem, ScheduleItem, RoomItem, PersonItem
from crawler.settings import LECTURE_SETTINGS, PERSON_SETTINGS, ROOM_SETTINGS
from crawler.spiders.base_spider import BaseSpider
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule
from scrapy.http import Request
from scrapy.selector import Selector


class LectureSpider(BaseSpider):
    """
    Spiders all modules from a given category
    """
    name = LECTURE_SETTINGS['name']
    allowed_domains = LECTURE_SETTINGS['allowed_domains']
    rules = (
        # Allows only to follow lecture details page
        Rule(SgmlLinkExtractor(restrict_xpaths=LECTURE_SETTINGS['xpath_lecture_details']),
             callback='parse_lecture_details',
             follow=False),
    )

    def __init__(self, module, *args, **kwargs):
        super(LectureSpider, self).__init__(*args, **kwargs)
        self.module = module
        self.start_urls = [module.uzh_url]

    def parse_lecture_details(self, response):
        """
        Extracts all information for a lecture details
        :param response: current context provided by scrapy
        :return: an item filled with all information found
        """
        sel = Selector(response)
        lecture = LectureItem()
        self._extract_xpath_from_dict(sel, LECTURE_SETTINGS['xpaths_to_extract'], lecture)
        lecture['uzh_id'] = self._extract_uzh_id(response.url)
        lecture['uzh_url'] = response.url
        uzh_url_schedule = self._get_uzh_url_schedule(response.url)
        yield Request(uzh_url_schedule, callback=self.parse_lecture_schedule, meta={'lecture': lecture})

    def parse_lecture_schedule(self, response):
        """
        Extracts all information regarding the schedules of a lecture
        :param response: response with an item meta attribute
        :return: item with schedules information
        """
        sel = Selector(response)
        lecture = response.meta['lecture']
        base_url = response.url.rsplit('/', 1)[0]
        lecture['schedule'] = []
        schedule_trs = sel.xpath(LECTURE_SETTINGS['xpath_lecturer_schedules'])
        for schedule_tr in schedule_trs:
            schedule_item = ScheduleItem()
            room_sels = schedule_tr.xpath(LECTURE_SETTINGS['rel_xpath_rooms'])
            lecturer_sels = schedule_tr.xpath(LECTURE_SETTINGS['rel_xpath_lecturers'])
            schedule_item['date'] = self._extract_xpath(schedule_tr, LECTURE_SETTINGS['rel_xpath_schedule_date'])
            schedule_item['time'] = self._extract_xpath(schedule_tr, LECTURE_SETTINGS['rel_xpath_schedule_time'])

            # crawl person(s) and room(s) to extract needed information
            schedule_item['room_ids'] = []
            schedule_item['room_items'] = []
            schedule_item['lecturer_ids'] = []
            schedule_item['lecturer_items'] = []

            for room_sel in room_sels:
                room_url = base_url + '/' + self._extract_xpath(room_sel, '@href')
                room_uzh_id = self._extract_uzh_id(room_url)
                schedule_item['room_ids'].append(room_uzh_id)
                yield Request(room_url, callback=self.parse_room_info, meta={'schedule_item': schedule_item})

            for lecturer_sel in lecturer_sels:
                lecturer_url = base_url + '/' + self._extract_xpath(lecturer_sel, '@href')
                lecturer_uzh_id = self._extract_uzh_id(lecturer_url)
                schedule_item['lecturer_ids'].append(lecturer_uzh_id)
                yield Request(lecturer_url, callback=self.parse_lecturer_info, meta={'schedule_item': schedule_item})

            lecture['schedule'].append(dict(schedule_item))

        yield lecture

    def parse_room_info(self, response):
        """
        Extracts information room from each schedule item
        :param response:
        """
        sel = Selector(response)
        schedule_item = response.meta['schedule_item']
        room_item = RoomItem()
        room_item['uzh_id'] = self._extract_uzh_id(response.url)
        room_item['uzh_url'] = response.url
        self._extract_xpath_from_dict(sel, ROOM_SETTINGS['xpaths_to_extract'], room_item)
        room_item['address'] = ' '.join(room_item['address'].split())  # remove unwanted whitespaces in address
        schedule_item['room_items'].append(room_item)

    def parse_lecturer_info(self, response):
        """
        Parses additional info of the lecturer
        :param response:
        :return:
        """
        sel = Selector(response)
        schedule_item = response.meta['schedule_item']
        person_item = PersonItem()
        person_item['uzh_id'] = self._extract_uzh_id(response.url)
        person_item['uzh_url'] = response.url
        self._extract_xpath_from_dict(sel, PERSON_SETTINGS['xpaths_to_extract'], person_item)
        schedule_item['lecturer_items'].append(dict(person_item))

    @staticmethod
    def _get_uzh_url_schedule(uzh_url_lecture_details):
        return uzh_url_lecture_details.replace(LECTURE_SETTINGS['lecture_details_url_id'],
                                               LECTURE_SETTINGS['lecture_schedule_url_id'])
