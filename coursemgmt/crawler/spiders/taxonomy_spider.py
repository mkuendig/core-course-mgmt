"""
Spiders the whole left navigation tree to create a taxonomy of the course categories.
"""
from crawler.items import CategoryItem
from crawler.settings import TAXONOMY_SETTINGS
from crawler.spiders.base_spider import BaseSpider
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule
from scrapy.selector import Selector
from scrapy.http import Request


class TaxonomySpider(BaseSpider):
    """
    Spiders the whole left navigation tree to create a taxonomy of the course categories.
    """

    name = TAXONOMY_SETTINGS['name']
    allowed_domains = TAXONOMY_SETTINGS['allowed_domains']
    rules = (
        # Allows only to follow links in the navigation and excludes all modules
        Rule(SgmlLinkExtractor(restrict_xpaths=TAXONOMY_SETTINGS['xpath_navigation'],
                               deny=TAXONOMY_SETTINGS['module_prefix']),
             callback='parse_taxonomy_item',
             follow=True),
    )

    def __init__(self, semester_name, *args, **kwargs):
        super(TaxonomySpider, self).__init__(*args, **kwargs)
        self.semester = semester_name
        self.start_urls = [TAXONOMY_SETTINGS['start_url'].format(semester=semester_name)]
        # self.start_urls = ['http://www.vorlesungen.uzh.ch/fs13/lehrangebot/fak-50000003/sc-50457276.html']

    def parse_taxonomy_item(self, response):
        """
        Extracts all information for a specific category item
        :param response: current context provided by scrapy
        :return: an item filled with all information found
        """
        sel = Selector(response)
        item = CategoryItem()
        item['uzh_id'] = self._extract_uzh_id(response.url)
        item['uzh_url'] = response.url
        item['title'] = self._extract_xpath(sel, TAXONOMY_SETTINGS['xpath_title'])
        item['parent_uzh_id'] = self._extract_parent_uzh_id(response)
        item['level'] = self._extract_level(response)
        item['study_degree'] = ''
        item['study_content'] = ''
        item['total_ects'] = ''
        item['major_minor_regulation'] = ''
        item['precognition'] = ''
        item['field_of_study'] = ''
        category_details_url = response.url.replace(TAXONOMY_SETTINGS['category_modules_url_id'],
                                                    TAXONOMY_SETTINGS['category_details_url_id'])
        if TAXONOMY_SETTINGS['category_details_url_id'] not in category_details_url:
            return item
        else:
            request = Request(category_details_url, callback=self.parse_category_details, meta={'item': item})
            return request

    def parse_category_details(self, response):
        """
        Extracts all information regarding the schedules of a lecture
        :param response: response with an item meta attribute
        :return: item with schedules information
        """
        sel = Selector(response)
        item = response.meta['item']
        item = self._extract_xpath_from_dict(sel, TAXONOMY_SETTINGS['xpaths_to_extract'], item)
        return item

    @staticmethod
    def _extract_parent_uzh_id(response):
        parent = response.url.split('/')[-2].split('.')[0]
        if '-' in parent:
            return parent
        else:
            return None

    @staticmethod
    def _extract_level(response):
        hierachy = response.url.split(TAXONOMY_SETTINGS['root_teaching'])[1]
        return len(hierachy.split('/'))
