"""
Spiders all modules from a given category
"""
from crawler.items import ModuleItem, PersonItem
from crawler.settings import MODULE_SETTINGS, PERSON_SETTINGS
from crawler.spiders.base_spider import BaseSpider
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule
from scrapy.selector import Selector
from scrapy.http import Request


class ModuleSpider(BaseSpider):
    """
    Spiders all modules from a given category
    """

    name = MODULE_SETTINGS['name']
    allowed_domains = MODULE_SETTINGS['allowed_domains']
    rules = (
        # Allows only to follow the pagination of the module list
        Rule(SgmlLinkExtractor(restrict_xpaths=MODULE_SETTINGS['xpath_pagination']),
             follow=True),
        # Gets all modules of listed in the module overview list
        Rule(SgmlLinkExtractor(restrict_xpaths=MODULE_SETTINGS['xpath_modules_list'],
                               allow=MODULE_SETTINGS['module_prefix']),
             callback='parse_modules_list',
             follow=False),
    )

    def __init__(self, category, *args, **kwargs):
        super(ModuleSpider, self).__init__(*args, **kwargs)
        self.category = category
        self.start_urls = [category.uzh_url]

    def parse_modules_list(self, response):
        """
        Extracts all information for a specific taxonomy item
        :param response: current context provided by scrapy
        :return: an item filled with all information found
        """
        sel = Selector(response)
        module = ModuleItem()
        module['uzh_id'] = self._extract_uzh_id(response.url)
        module['uzh_url'] = response.url
        module['person_items'] = []
        module['person_ids'] = []
        module['organizational_units'] = []
        module = self._extract_xpath_from_dict(sel, MODULE_SETTINGS['xpaths_to_extract'], module)

        # crawl person(s) to extract info about person(s) in charge (Modulverantwortlicher)
        person_sels = sel.xpath(MODULE_SETTINGS['persons_in_charge'])
        base_url = response.url.rsplit('/', 1)[0]
        for person_sel in person_sels:
            person_in_charge_url = base_url + '/' + self._extract_xpath(person_sel, '@href')
            person_in_charge_uzh_id = self._extract_uzh_id(person_in_charge_url)
            module['person_ids'].append(person_in_charge_uzh_id)
            yield Request(person_in_charge_url, callback=self.parse_person_info, meta={'module': module})

        organizational_units_sels = sel.xpath(MODULE_SETTINGS['organizational_units'])
        for organizational_units_sel in organizational_units_sels:
            url = self._extract_xpath(organizational_units_sel, '@href')
            title = self._extract_xpath(organizational_units_sel, 'text()')
            organizational_unit = {'title': title, 'url': url}
            module['organizational_units'].append(organizational_unit)

        yield module

    def parse_person_info(self, response):
        sel = Selector(response)
        module = response.meta['module']
        person_item = PersonItem()
        person_item['uzh_id'] = self._extract_uzh_id(response.url)
        person_item['uzh_url'] = response.url
        self._extract_xpath_from_dict(sel, PERSON_SETTINGS['xpaths_to_extract'], person_item)
        module['person_items'].append(dict(person_item))

