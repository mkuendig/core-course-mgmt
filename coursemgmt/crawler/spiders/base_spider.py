"""
Base class for all spiders
"""
from abc import ABCMeta
from scrapy.contrib.spiders import CrawlSpider


class BaseSpider(CrawlSpider):
    """
    Base class for all spiders
    """

    # Abstract Base Class
    __metaclass__ = ABCMeta

    def __init__(self, *args, **kwargs):
        super(BaseSpider, self).__init__(*args, **kwargs)

    @staticmethod
    def _extract_uzh_id(url):
        """
        Extracts the id of a category item.
        Examples:
        http://www.vorlesungen.uzh.ch/HS14/lehrangebot/fak-50000001/sc-50306207.html --> sc-50306207
        http://www.vorlesungen.uzh.ch/HS14/lehrangebot/fak-50000003/sc-50332888/cga-50332888010/cg-50463944.module.html --> cg-50463944

        :param response: response provided by scrapy
        :return: the id of a category item
        """
        return url.split('/')[-1].split('.')[0]

    @staticmethod
    def _extract_xpath(sel, xpath):
        """
        Returns the value for a given xpath expression and selector

        :param sel: scrapy selector
        :param xpath: xpath expression
        :return: extracted value
        """
        extracted_content = sel.xpath(xpath).extract()
        if len(extracted_content):
            return ' '.join(extracted_content).strip()
        else:
            return ''

    @staticmethod
    def _extract_xpath_from_dict(sel, xpath_dict, item):
        """
        Fills a given item with extracted values

        :param sel: scrapy selector
        :param xpath_dict: key value pair of item key and xpath value
        :param item: item to be filled
        :return: filled item
        """

        for (key, xpath) in xpath_dict.iteritems():
            item[key] = BaseSpider._extract_xpath(sel, xpath)

        return item
