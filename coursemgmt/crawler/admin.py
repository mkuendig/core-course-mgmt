"""
Admin interfaces for the models.
Uses django-reversion to have a versioning of the models
"""
from django.contrib import admin
from django.contrib.admin import StackedInline, TabularInline
from crawler.models import Semester, Category, Module, Lecture, LectureScheduleItem, Person, Room, ModuleFeature, \
    CrawlJob, ModuleOrganizational, ExportJob, CustomCategory, LectureCategory, TagCategoriesJob
import reversion


class BaseAdmin(reversion.VersionAdmin):
    list_per_page = 1000
    change_list_template = "admin/change_list_filter_sidebar.html"
    change_list_filter_template = "admin/filter_listing.html"


class CrawlJobAdmin(BaseAdmin):
    """
    Admin for the crawl job
    """
    readonly_fields = ['start_date', 'end_date', 'is_finished']
    list_filter = ['command']
    list_display = ['command', 'semester', 'start_date', 'end_date', 'is_finished']
    actions = None


admin.site.register(CrawlJob, CrawlJobAdmin)


class ExportJobAdmin(BaseAdmin):
    """
    Admin for export jobs
    """
    readonly_fields = ['start_date', 'end_date', 'is_finished']
    list_filter = ['command']
    list_display = ['command', 'start_date', 'end_date', 'is_finished']
    actions = None


admin.site.register(ExportJob, ExportJobAdmin)


class TagCategoriesJobAdmin(BaseAdmin):
    """
    Admin for tagging categories
    """
    readonly_fields = ['start_date', 'end_date', 'is_finished']
    list_filter = ['command']
    list_display = ['command', 'start_date', 'end_date', 'is_finished']
    actions = None


admin.site.register(TagCategoriesJob, TagCategoriesJobAdmin)


class SemesterAdmin(BaseAdmin):
    """
    Admin for the semester
    """
    pass


admin.site.register(Semester, SemesterAdmin)


class CategoryAdmin(BaseAdmin):
    """
    Admin for Category
    """
    search_fields = ['title', 'uzh_id']
    list_filter = ['semester']
    list_display = ['title', 'uzh_id', 'level']
    list_display_links = ['title', 'uzh_id']
    filter_horizontal = ['parents', 'custom_categories']
    ordering = ['level', 'title']


admin.site.register(Category, CategoryAdmin)


class CustomCategoryAdmin(BaseAdmin):
    """
    Admin for Custom Category
    """
    list_display = ['title', 'slug', 'module_type']


admin.site.register(CustomCategory, CustomCategoryAdmin)


class LectureInlineAdmin(StackedInline):
    """
    Inline admin for lecture
    """
    model = Lecture
    extra = 0


class ModuleFeatureInlineAdmin(TabularInline):
    """
    Inline admin for lecture
    """
    model = Module.features.through
    extra = 0


class ModuleAdmin(BaseAdmin):
    """
    Admin for the module
    """
    list_per_page = 1000
    list_filter = ['semester', 'organizational_units']
    search_fields = ['title', 'uzh_id']
    list_display = ['title', 'uzh_code', 'semester']
    filter_horizontal = ['categories', 'persons_in_charge']
    inlines = [
        LectureInlineAdmin,
        ModuleFeatureInlineAdmin
    ]


admin.site.register(Module, ModuleAdmin)


class ModuleOrganizationalAdmin(BaseAdmin):
    """
    Admin for the module organizational
    """
    list_display = ['title', 'url']


admin.site.register(ModuleOrganizational, ModuleOrganizationalAdmin)


class LectureScheduleInlineAdmin(TabularInline):
    """
    Inline admin for lecture
    """
    model = LectureScheduleItem
    extra = 0


class LectureAdmin(BaseAdmin):
    """
    Admin for the lecture
    """
    list_per_page = 1000
    list_filter = ['semester', 'category']
    search_fields = ['title', 'uzh_id']
    list_display = ['title', 'uzh_id', 'semester']
    inlines = [
        LectureScheduleInlineAdmin,
    ]


admin.site.register(Lecture, LectureAdmin)


class LectureCategoryAdmin(BaseAdmin):
    """
    Admin for lecturer categories
    """
    list_display = ['title', 'is_active']


admin.site.register(LectureCategory, LectureCategoryAdmin)


class PersonAdmin(BaseAdmin):
    """
    Admin for persons
    """
    list_per_page = 1000
    list_display = ['firstname', 'lastname', 'title', 'uzh_id']
    list_filter = ['semester']
    search_fields = ['firstname', 'lastname']


admin.site.register(Person, PersonAdmin)


class RoomAdmin(BaseAdmin):
    """
    Admin for rooms
    """
    list_per_page = 1000
    list_display = ['name', 'street', 'address', 'uzh_id']
    list_filter = ['semester', 'street']
    search_fields = ['name']


admin.site.register(Room, RoomAdmin)


class ModuleFeatureAdmin(BaseAdmin):
    """
    Admin for features
    """
    list_per_page = 1000
    list_display = ['title']
    search_fields = ['title']


admin.site.register(ModuleFeature, ModuleFeatureAdmin)
