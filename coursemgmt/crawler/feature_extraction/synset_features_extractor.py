"""
Extract features from all modules and score each feature of each module accordingly
"""
import operator

from crawler.feature_extraction.lda_features_extractor import LdaFeaturesExtractor
from crawler.feature_extraction.module_preprocessor import ModulePreprocessor
from crawler.models import Module, ModuleFeatureRelation, ModuleFeature
from textblob import Word
from textblob.wordnet import NOUN


class SynsetFeaturesExtractor(object):
    module_synsets = {}  # set with module as keys array of synsets as values
    synset_counter = {}  # set with synsets as key and an integer as value
    module_features = {}  # set with uzh_code of modules as key and list of 500 dictionaries with feature name and value
    most_frequent_synsets = {}  #
    modules = Module.objects.filter(lectures__category__is_active=True).order_by('uzh_code', '-semester').distinct(
            'uzh_code')
    preprocessor = ModulePreprocessor()

    def __init__(self):
        # self.preprocessor.process_module_descriptions(self.modules)

        self.calculate_lda_module_synsets()
        for synset in self.most_frequent_synsets:
            print(synset)
            # self.calculate_topics()

    def calculate_lda_module_synsets(self):
        topics = LdaFeaturesExtractor(topics=20, words_per_topic=2).calculate_lda_topics()
        for key, topics in topics.iteritems():
            for topic in topics:
                word = Word(topic)
                if len(word.get_synsets(pos=NOUN)) > 0:
                    self.synset_counter[word.get_synsets(pos=NOUN)[0]] = 1
                    # for synset in word.synsets:
                    #     if synset.name()[-4:] == "n.01":
                    #         self.synset_counter[synset] = 1
        self.most_frequent_synsets = dict(
                sorted(self.synset_counter.iteritems(), key=operator.itemgetter(1), reverse=True)[:500])

        for module in self.modules:
            module_synsets = []
            for word in module.processed_description.split(" "):
                word = Word(word)
                for synset in word.synsets:
                    if synset in self.most_frequent_synsets:
                        if synset not in module_synsets:
                            module_synsets.append(synset)
            self.module_synsets[module] = module_synsets

    def calculate_module_synsets(self):
        for module in self.modules:
            module_synsets = []
            for word in module.processed_description.split(" "):
                word = Word(word)
                for synset in word.synsets:
                    if synset in self.synset_counter:
                        self.synset_counter[synset] += 1
                    elif synset.name()[-2:] == "01":  # only consider most relevant synset
                        # else:
                        self.synset_counter[synset] = 1
                        module_synsets.append(synset)
            self.module_synsets[module] = module_synsets
        self.most_frequent_synsets = dict(
                sorted(self.synset_counter.iteritems(), key=operator.itemgetter(1), reverse=True)[:500])

    def calculate_topics(self):
        number_of_topics = 10
        topics = []
        for _ in range(number_of_topics):
            topics.append(set())
        counter = 0
        for synset, count in self.most_frequent_synsets.items():
            if count > 3:
                topics[counter % number_of_topics].update([synset])
                counter += 1

        for _ in range(100):
            for synset in self.most_frequent_synsets.keys():
                best_topic_index = None
                best_topic_similarity = 0
                for index, topic in enumerate(topics):
                    if synset in topic:
                        topic.remove(synset)
                    similarity = self.calculate_similarity(synset, topic)
                    if similarity > best_topic_similarity:
                        best_topic_similarity = similarity
                        best_topic_index = index
                topics[best_topic_index].update([synset])
            for topic in topics:
                print topic
            print("-------------")

    def calculate_module_keywords(self):
        ModuleFeatureRelation.objects.all().delete()
        ModuleFeature.objects.all().delete()

        for module, module_synsets in self.module_synsets.items():
            for frequent_synset in self.most_frequent_synsets:
                feature_similarity = self.calculate_similarity(frequent_synset, module_synsets)
                if feature_similarity != 0:
                    feature_name = frequent_synset.name()
                    feature, _ = ModuleFeature.objects.get_or_create(title=feature_name)
                    module_feature_relation = ModuleFeatureRelation(module=module, feature=feature,
                                                                    weight=feature_similarity)
                    module_feature_relation.save()
            module.save()
            print("Saved module - " + module.title)

    @staticmethod
    def calculate_similarity(synset, synset_collection):
        counter = 0
        similarity_sum = 0
        for collection_synset in synset_collection:
            path_similarity = collection_synset.wup_similarity(synset)
            if path_similarity is not None:
                similarity_sum += path_similarity
                counter += 1
        return similarity_sum / counter if counter != 0 else 0

    def calculate_module_similarity(self, module_title_1, module_title_2):
        def get_synsets(text):
            synsets = []
            for word in text.split(" "):
                textblob_word = Word(word)
                for synset in textblob_word.get_synsets():
                    synsets.append(synset)
            return synsets

        module_1_synsets = get_synsets(module_title_1)
        module_2_synsets = get_synsets(module_title_2)

        for module_word in module_title_2.split(" "):
            word = Word(module_word)
            if len(word.get_synsets('n')) > 0:
                module_2_synsets.append(word.get_synsets('n')[0])
            if len(word.get_synsets('a')) > 0:
                module_2_synsets.append(word.get_synsets('a')[0])

        print("\n\n***** Module 1 *****")
        print(module_title_1)
        print("***** Synsets *****")
        print(module_1_synsets)

        print("\n\n***** Module 2 *****")
        print(module_title_2)
        print("***** Synsets *****")
        print(module_2_synsets)

        similarity_sum = 0
        counter = 0
        for synset in module_1_synsets:
            similarity_sum += self.calculate_similarity(synset, module_2_synsets)
            counter += 1
        return similarity_sum / counter
