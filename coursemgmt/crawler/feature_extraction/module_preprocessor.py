import nltk
import os.path
import textblob
from textblob import TextBlob, Word
from textblob.utils import strip_punc
from nltk.stem.porter import PorterStemmer


class ModulePreprocessor:
    def __init__(self):
        self.stop_words = self.read_stop_words()

    def process_module_descriptions(self, modules):
        """
        Go through all recent, active modules and extract the processed description from the module title and description
        text
        :param modules: crawler.models.Module objects
        """
        print("Preprocess modules")
        for module in modules:
            module_text = module.title + " " + module.description + " " + module.target_audience + " " + module.precognition + " " + module.teaching_materials
            module.processed_description = self._translate_and_process(module_text)
            module.save()

    def _translate_and_process(self, text):
        """
        Removes unnecessary words from text and translates text to english
        :param text: string
        :return: TextBlob object
        """

        blob = TextBlob(text)
        try:
            en_blob = blob.translate(from_lang="de", to="en")
        except textblob.exceptions.NotTranslated:  # NotTranslated exception is thrown when translated blob stayed the same
            en_blob = blob

        words = en_blob.words

        # add noun phrases
        # noun_phrases = blob.noun_phrases
        # for noun_phrase in noun_phrases:
        #     words.append(noun_phrase)

        # remove punctuation, then lemmatize, strip, and lowercase words
        words_set = []
        porter_stemmer = PorterStemmer()
        for word in words:
            word = Word(strip_punc(word))
            # word = word.strip().lower()
            # word = porter_stemmer.stem(word)
            word = word.lemmatize().strip().lower()
            words_set.append(word)

        # remove stop words, digits, and words with less than 2 letters
        cleaned_words = []
        for word in words_set:
            if word not in nltk.corpus.stopwords.words('english') and len(
                    word) >= 3 and not word.isdigit():
                cleaned_words.append(word)

        # remove unnecessary words
        for word in cleaned_words:
            if word in self.stop_words:
                cleaned_words.remove(word)

        processed_description = ' '.join(cleaned_words)
        return processed_description

    @staticmethod
    def read_stop_words():
        base = os.path.dirname(os.path.abspath(__file__))
        f = open(os.path.join(base, "stopwords.txt"))
        stop_words = []
        for stop_word in f.read().split(" "):
            stop_words.append(stop_word)
        return stop_words
