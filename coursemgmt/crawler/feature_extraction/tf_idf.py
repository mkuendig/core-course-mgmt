"""
Extract keywords from a given text. Uses td-idf to weight extracted keywords
"""
import math
from collections import Counter

from crawler.models import Module, ModuleFeatureRelation, ModuleFeature


class TfIdf(object):
    """
    Extract keywords from a given text. Uses td-idf to weight extracted keywords
    """
    corpus = {}
    words_in_corpus = Counter()
    words_per_doc = []
    nr_docs = 0
    modules = Module.objects.filter(lectures__category__is_active=True).order_by('uzh_code', '-semester').distinct(
            'uzh_code')

    def __init__(self, max_keywords=1000):
        # ModulePreprocessor().process_module_descriptions(self.modules)
        for module in self.modules:
            self._add_document_to_corpus(module, module.processed_description)
        self.results = self.process(max_keywords)

    def _add_document_to_corpus(self, module, document_text):
        """
        Add weighted text list to corpus. List should contain a tuple (text, weight (int))
        :param module: identfier for the the texts
        :param document_text: string of text
        :return:
        """
        self.corpus[module] = document_text
        for word in document_text.split(" "):
            self.words_in_corpus.update({word: 1})

        # number of different keys in the entire corpus
        self.nr_docs = float(len(self.corpus))

    @staticmethod
    def _tf(number_word_occurrences, total_number_of_words):
        """
        Calculate term frequency of a word
        :param number_word_occurrences: nr occurence of word in document
        :param total_number_of_words: total number of words in document
        :return: term frequency
        """
        return float(number_word_occurrences) / total_number_of_words

    def _n_containing(self, word):
        """
        Calculates in how many documents a word occurs
        :param word: word to be checked
        :return: number how many documents the word was found
        """
        return sum(1 for key, words in self.words_per_doc if word in words)

    def _idf(self, word):
        """
        Calculates the invert document frequency of a word
        :param word: word to be checked
        :return: inverse document frequency
        """
        return math.log(self.nr_docs / (1 + self._n_containing(word)))

    def _tfidf(self, word, number_word_occurrences, total_number_of_words):
        """
        Calculate the term frequency - invert document frequency of a word
        :param word: word to be checked
        :param number_word_occurrences: nr occurrence of word in document
        :param total_number_of_words: total number of words in document
        :return: term frequency - invert document frequency
        """
        return self._tf(number_word_occurrences, total_number_of_words) * self._idf(word)

    @staticmethod
    def _convert_to_counter(text):
        """
        Convert a word list to a counter object with word as key and number of occurrence as value
        :param words: word list
        :return: counter object
        """
        counter = Counter()
        for word in text.split(" "):
            counter.update({word: 1})
        return counter

    def process(self, max_keywords):
        print("process tf-idf")

        # go through every key in the corpus
        for key, document_text in self.corpus.iteritems():
            tokens = self._convert_to_counter(document_text)
            self.words_per_doc.append((key, tokens))

        results = {}
        for module, words in self.words_per_doc:
            total_number_of_words = sum(words.values())
            counter = Counter()
            for word, number_word_occurrences in words.iteritems():
                counter.update({word: self._tfidf(word, number_word_occurrences, total_number_of_words)})
            tf_idf_words = ""
            for word, factor in counter.most_common(max_keywords):
                tf_idf_words += word + " "
            module.tf_idf_words = tf_idf_words[:-1]
            module.save()
            results[module] = counter.most_common(max_keywords)
        return results

    def get_results(self):
        return self.results

    def calculate_module_features(self):
        print("updating module features")

        # delete old ModuleFeatures
        ModuleFeatureRelation.objects.all().delete()
        ModuleFeature.objects.all().delete()

        for module in self.results:
            for word_tuple in self.results[module]:
                feature, _ = ModuleFeature.objects.get_or_create(title=word_tuple[0])
                module_feature_relation = ModuleFeatureRelation(module=module, feature=feature, weight=word_tuple[1])
                module_feature_relation.save()
