from collections import Counter

import numpy as np
from crawler.feature_extraction.module_preprocessor import ModulePreprocessor
from crawler.models import Module, ModuleFeatureRelation, ModuleFeature
from lda.lda import LDA
from sklearn.feature_extraction.text import CountVectorizer


class LdaFeaturesExtractor:
    modules = Module.objects.filter(lectures__category__is_active=True).order_by('uzh_code', '-semester') \
        .distinct('uzh_code')
    preprocessor = ModulePreprocessor()

    def __init__(self, topics=75, words_per_topic=3, min_df=0.005, max_df=0.4):
        self.min_df = min_df
        self.max_df = max_df
        self.number_of_topics = topics
        self.number_of_words_per_topic = words_per_topic
        # self.preprocessor.process_module_descriptions(self.modules)

    def extract_lda_features(self):

        # delete old ModuleFeatures
        ModuleFeatureRelation.objects.all().delete()
        ModuleFeature.objects.all().delete()

        # print("Process module descriptions")
        # self.preprocessor.process_module_descriptions(self.modules)

        topics = self.calculate_lda_topics()

        # go through each module and save the topic percentage (based how many times a word in the module matches a
        # word in a topic) as a module feature relation
        for module in self.modules:
            topic_counter = Counter()
            word_count = 0
            for topic_name, topic_words in topics.iteritems():
                for word in module.processed_description.split(" "):
                    if word in topic_words:
                        topic_counter.update({topic_name: 1})
                        word_count += 1

            # total_found_topics = sum(topic_counter.values())
            for topic_name, count in topic_counter.iteritems():
                feature, _ = ModuleFeature.objects.get_or_create(title=topic_name)
                # weight = float(count) / total_found_topics
                weight = float(count) / word_count
                module_keyword_relation = ModuleFeatureRelation(module=module, feature=feature, weight=weight)
                module_keyword_relation.save()

    def calculate_lda_topics(self):
        module_descriptions = []
        for module in self.modules:
            module_descriptions.append(module.processed_description)

        # max_df tells that words can occur in max 5% of all documents (approx 30 module desc), and min_df that it has
        # to occur in at least 0.5 percent of the documents (approx 5 module desc)
        vectorizer = CountVectorizer(analyzer='word', stop_words=self.preprocessor.stop_words, max_df=self.max_df,
                                     min_df=self.min_df)
        features_matrix = vectorizer.fit_transform(module_descriptions)
        features_array = features_matrix.toarray()
        vocab = vectorizer.get_feature_names()
        model = LDA(n_topics=self.number_of_topics, n_iter=600)
        model.fit(features_array)
        topic_word = model.topic_word_
        topics = {}
        for i, topic_dist in enumerate(topic_word):
            topic_words = np.array(vocab)[np.argsort(topic_dist)][:-(self.number_of_words_per_topic + 1):-1]
            topics[u'topic' + str(i + 1) + u': ' + u' '.join(topic_words)] = topic_words
            print(u'Topic {}: {}'.format(i + 1, u' '.join(topic_words)))
        return topics
