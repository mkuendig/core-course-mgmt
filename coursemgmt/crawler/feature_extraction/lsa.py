import logging
from collections import Counter
from operator import itemgetter

from crawler.feature_extraction.module_preprocessor import ModulePreprocessor
from crawler.models import Module, ModuleFeature, ModuleFeatureRelation
from gensim import corpora
from gensim.models import LsiModel

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


class LsaFeaturesExtractor:
    modules = Module.objects.filter(lectures__category__is_active=True).order_by('uzh_code', '-semester') \
        .distinct('uzh_code')
    preprocessor = ModulePreprocessor()

    def __init__(self, number_of_topics=500, number_of_features=25):

        self.number_of_topics = number_of_topics
        self.number_of_features = number_of_features

        # delete old ModuleFeatures
        ModuleFeatureRelation.objects.all().delete()
        ModuleFeature.objects.all().delete()

        documents = []
        for module in self.modules:
            documents.append(module.processed_description.split(" "))
        dictionary = corpora.Dictionary(documents)
        corpus = [dictionary.doc2bow(text) for text in documents]
        lsi = LsiModel(corpus, id2word=dictionary, num_topics=self.number_of_topics)
        lsi.print_topics(num_topics=self.number_of_topics)

        topic_counter = Counter()
        for module in self.modules:
            topics = lsi[dictionary.doc2bow(module.processed_description.split(" "))]
            best_topic = max(topics, key=itemgetter(1))[0]
            if best_topic == 0:
                try:
                    best_topic = max(topics, key=itemgetter(2))[0]
                except IndexError:
                    pass

            topic_counter.update({best_topic: 1})
            print(module.title)
            print(best_topic)
            print(topic_counter.most_common())
            best_topic = lsi.show_topic(best_topic, topn=self.number_of_features)
            for word, factor in best_topic:
                feature, _ = ModuleFeature.objects.get_or_create(title=word)
                module_feature_relation = ModuleFeatureRelation(module=module, feature=feature, weight=factor)
                module_feature_relation.save()
