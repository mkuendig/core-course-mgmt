"""
Takes care of the save process of the lecture details
"""
import reversion
from base_pipeline import BasePipeline
from crawler.models import Lecture
from crawler.models import LectureScheduleItem
from crawler.models import Person
from crawler.models import Room
from crawler.settings import LECTURE_PIPELINE_SETTINGS
from django.db import transaction
from django.utils import timezone
from django.utils.datetime_safe import datetime
from scrapy import log


class LecturePipeline(BasePipeline):
    """
    Takes care of the save process of the lecture details
    """

    def close_spider(self, spider):
        """
        After the spidering process save all items
        :param spider: spider which has processed the item
        """
        semester = spider.module.semester

        log.msg('Insert or update all lectures in DB')
        for item in self.items:
            for lecture_schedule_item in item['schedule']:
                for lecturer_item in lecture_schedule_item['lecturer_items']:
                    Person.create_or_update(semester,
                                            lecturer_item['uzh_id'],
                                            lecturer_item['uzh_url'],
                                            lecturer_item['firstname'],
                                            lecturer_item['lastname'],
                                            lecturer_item['title'],
                                            lecturer_item['position'])

                for room_item in lecture_schedule_item['room_items']:
                    Room.create_or_update(semester,
                                          room_item['uzh_id'],
                                          room_item['uzh_url'],
                                          room_item['name'],
                                          room_item['street'],
                                          room_item['address'])

            lecture = Lecture.create_or_update(item['uzh_id'], item['uzh_url'], item['title'], item['uzh_lecture_id'],
                                               item['uzh_lecture_code'], item['category'], item['content'],
                                               item['teaching_materials'], semester, spider.module)
            # reversion support
            with transaction.atomic(), reversion.create_revision():
                for lecture_schedule_item in item['schedule']:
                    start_datetime, end_datetime = self._get_start_end_datetime(lecture_schedule_item)
                    LectureScheduleItem.create_or_update(lecture=lecture, start_datetime=start_datetime,
                                                         end_datetime=end_datetime,
                                                         room_ids=lecture_schedule_item['room_ids'],
                                                         lecturer_ids=lecture_schedule_item['lecturer_ids'])

    def _get_start_end_datetime(self, schedule_item):
        date_string = schedule_item['date']
        # remove weekday
        date_string = date_string.split(' ')[1]

        # split start and end time
        time_string = schedule_item['time'].split(' - ')
        start_time_string = time_string[0]
        end_time_string = time_string[1]

        # convert to datetime objects and return both start and end datetime
        return self._parse_datetime(date_string, start_time_string), self._parse_datetime(date_string, end_time_string)

    @staticmethod
    def _parse_datetime(date_string, time_string):
        datetime_string = '{} {}'.format(date_string, time_string)
        current_tz = timezone.get_current_timezone()
        return current_tz.localize(datetime.strptime(datetime_string, LECTURE_PIPELINE_SETTINGS['date_format']))
