"""
Takes care of the save process of the taxonomy (categories)
"""
from base_pipeline import BasePipeline
from crawler.models import Module
from crawler.models import Person
from scrapy import log


class ModulePipeline(BasePipeline):
    """
    Takes care of the save process of the taxonomy (categories)
    """

    def close_spider(self, spider):
        """
        After the spidering process save all items
        :param spider: spider which has processed the item
        """
        semester = spider.category.semester

        log.msg('Insert or update all modules in DB')
        for item in self.items:
            for person_item in item['person_items']:
                Person.create_or_update(semester,
                                        person_item['uzh_id'],
                                        person_item['uzh_url'],
                                        person_item['firstname'],
                                        person_item['lastname'],
                                        person_item['title'],
                                        person_item['position'])

            Module.create_or_update(item['uzh_id'],
                                    item['uzh_url'],
                                    semester,
                                    item['title'],
                                    item['uzh_code'],
                                    spider.category,
                                    item['ects'],
                                    item['frequency'],
                                    item['description'],
                                    item['precognition'],
                                    item['requirements'],
                                    item['teaching_materials'],
                                    item['learning_target'],
                                    item['target_audience'],
                                    item['person_ids'],
                                    item['achievement_test'],
                                    item['grading_scale'],
                                    item['repeatability'],
                                    item['language'],
                                    item['further_information'],
                                    item['booking_deadline'],
                                    item['cancellation_deadline'],
                                    item['organizational_units'])
