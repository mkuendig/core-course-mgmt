"""
Base class for all pipelines
"""
from abc import ABCMeta


class BasePipeline(object):
    """
    Collects all item in a list
    """

    # Abstract Base Class
    __metaclass__ = ABCMeta

    def __init__(self):
        self.items = []

    def process_item(self, item, spider):
        """
        Add all items to a list
        :param item: Item to be added to the list
        :param spider: spider which has processed the item
        :return: same item
        """
        # add item to a list
        self.items.append(item)
        return item
