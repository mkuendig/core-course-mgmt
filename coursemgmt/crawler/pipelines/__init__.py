"""
Scrapy Pipelines

After an item has been scraped by a spider, it is sent to the Item Pipeline which process it through several
components that are executed sequentially.

See http://doc.scrapy.org/en/latest/topics/item-pipeline.html
"""
