"""
Takes care of the save process of the taxonomy (categories)
"""
from base_pipeline import BasePipeline
from crawler.models import Category
from crawler.models import Semester
from scrapy import log


class TaxonomyPipeline(BasePipeline):
    """
    Takes care of the save process of the taxonomy (categories)
    """

    def close_spider(self, spider):
        """
        After the spidering process save all items
        :param spider: spider which has processed the item
        """
        # create or update the semester django object
        log.msg('Insert or update semester in DB')
        semester = Semester.create_or_update(spider.semester)

        # firstly, create or update all information
        log.msg('Insert or update all categories in DB')
        for item in self.items:
            Category.create_or_update(item['uzh_id'],
                                      item['uzh_url'],
                                      item['title'],
                                      item['level'],
                                      semester,
                                      item['study_degree'],
                                      item['study_content'],
                                      item['total_ects'],
                                      item['major_minor_regulation'],
                                      item['precognition'],
                                      item['field_of_study'])

        # secondly, update the hierarchy of the categories
        log.msg('Update hierarchy of the categories (update parent category) in DB')
        for item in self.items:
            if item['parent_uzh_id']:
                category = Category.objects.get(uzh_id=item['uzh_id'], semester=semester)
                category.update_parent(item['parent_uzh_id'], semester)
