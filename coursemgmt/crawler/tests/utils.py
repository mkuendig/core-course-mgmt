"""
Utils for testing spiders
"""

from scrapy import log, signals
from scrapy.crawler import Crawler
from scrapy.log import DEBUG
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor


class ReactorControl(object):
    """
    Takes care that the reactor is stopped when all spiders are done
    """

    def __init__(self):
        self.crawlers_running = 0

    def add_crawler(self):
        """
        Register crawler
        """
        self.crawlers_running += 1

    def remove_crawler(self):
        """
        Remove crawler and stop reactor if it was last
        """
        self.crawlers_running -= 1
        if self.crawlers_running == 0:
            reactor.stop()


reactor_control = ReactorControl()


def setup_spider(spider, pipeline):
    settings = get_project_settings()
    settings.overrides['ITEM_PIPELINES'] = {
        pipeline: 1,
    }
    crawler = Crawler(settings)
    crawler.signals.connect(reactor_control.remove_crawler, signal=signals.spider_closed)
    crawler.configure()
    crawler.crawl(spider)
    crawler.start()
    reactor_control.add_crawler()


def run_all_spiders():
    log.start(loglevel=DEBUG)
    reactor.run()
