# coding=utf-8
"""
Test cases for all spiders
Unfortunately all spiders have to run in one testcase because only one reactor can be started

"""

from crawler.models.simple_attributes import Category, Semester, Module, Lecture, LectureScheduleItem, SimpleAttribute,\
    ModuleRepeatability, ModuleDeadline, ModuleLanguage, ModuleFrequency, ModuleGradingScale, ModuleOrganizational
from crawler.spiders.lecture_spider import LectureSpider
from crawler.spiders.module_spider import ModuleSpider
from crawler.spiders.taxonomy_spider import TaxonomySpider
from crawler.tests.utils import run_all_spiders, setup_spider
from django.test import TestCase
from django.utils import timezone
from django.utils.datetime_safe import datetime


def setUpModule():
    """
    Setup all spiders and start crawling
    Is executed only once
    """
    _setup_taxonomy_spider()
    _setup_module_spider()
    _setup_lecture_spider()
    run_all_spiders()


def _setup_taxonomy_spider():
    spider = TaxonomySpider(semester_name='fs14')
    setup_spider(spider, 'crawler.pipelines.taxonomy_pipeline.TaxonomyPipeline')


def _setup_module_spider():
    semester = Semester.create_or_update(semester_name='fs14')
    category = Category.create_or_update(uzh_id='cg-50332970',
                                         uzh_url='http://www.vorlesungen.uzh.ch/FS14/lehrangebot/fak-50000003/sc-50332888/cga-50332888010/cg-50463944/cg-50463947/cg-50332970.module.html',
                                         level=6,
                                         title='',
                                         semester=semester)
    spider = ModuleSpider(category=category)
    setup_spider(spider, 'crawler.pipelines.module_pipeline.ModulePipeline')

    category = Category.create_or_update(uzh_id='cg-50350080',
                                         uzh_url='http://www.vorlesungen.uzh.ch/FS14/lehrangebot/fak-50000003/sc-50332888/cga-50332888010/cg-50463944/cg-50463947/cg-50350080.module.html',
                                         level=6,
                                         title='',
                                         semester=semester)
    spider = ModuleSpider(category=category)
    setup_spider(spider, 'crawler.pipelines.module_pipeline.ModulePipeline')


def _setup_lecture_spider():
    semester = Semester.create_or_update(semester_name='fs14')
    category = Category.create_or_update(uzh_id='cg-50332970',
                                         uzh_url='http://www.vorlesungen.uzh.ch/FS14/lehrangebot/fak-50000003/sc-50332888/cga-50332888010/cg-50463944/cg-50463947/cg-50332970.module.html',
                                         level=6,
                                         title='',
                                         semester=semester)
    module = Module.create_or_update(uzh_id='sm-50031187',
                                     uzh_url='http://www.vorlesungen.uzh.ch/fs14/lehrangebot/fak-50000003/sc-50427948/cga-50427948010/cg-50427949/cg-50427957/cg-50428049/cg-50428310/sm-50031187.modveranst.html',
                                     semester=semester,
                                     title='Mobile Communication Syst. (L+E)',
                                     uzh_code='MINF4220',
                                     category=category)
    spider = LectureSpider(module=module)
    setup_spider(spider, 'crawler.pipelines.lecture_pipeline.LecturePipeline')

    module = Module.create_or_update(uzh_id='sm-50574529',
                                     uzh_url='http://www.vorlesungen.uzh.ch/FS14/lehrangebot/fak-50000003/sc-50332888/cga-50332888010/cg-50463944/cg-50463947/cg-50332970/sm-50574529.modveranst.html',
                                     title='Practical Artificial Intelligence (L+E)',
                                     semester=semester,
                                     uzh_code='MINF4529',
                                     category=category)
    spider = LectureSpider(module=module)
    setup_spider(spider, 'crawler.pipelines.lecture_pipeline.LecturePipeline')


class TestTaxonomySpider(TestCase):
    def test_total_amount_of_semesters(self):
        semesters = Semester.objects.all()
        self.assertEqual(len(semesters), 1)

    def test_semester_title(self):
        semester = Semester.objects.first()
        self.assertEqual(semester.title, 'fs14')

    def test_total_amount_of_categories(self):
        categories = Category.objects.all()
        self.assertEqual(len(categories), 277)

    def test_root_node(self):
        root = Category.objects.get(level=1)
        self.assertEqual(root.uzh_id, 'fak-50000003')

    def test_category_level(self):
        category = Category.objects.get(uzh_id='cg-50332890')
        self.assertEqual(category.level, 5)
        category = Category.objects.get(uzh_id='cg-50578609')
        self.assertEqual(category.level, 6)

    def test_parents(self):
        category = Category.objects.get(uzh_id='cg-50350079')
        all_parents = category.parents.all()
        self.assertEqual(len(all_parents), 2)

        first_parent = category.parents.filter(uzh_id='cg-50332892')
        self.assertEqual(len(first_parent), 1)

        second_parent = category.parents.filter(uzh_id='cg-50463947')
        self.assertEqual(len(second_parent), 1)

    def test_children(self):
        category = Category.objects.get(uzh_id='cg-50463947')
        children = category.children.all()
        self.assertEqual(len(children), 6)

    def test_url(self):
        category = Category.objects.get(uzh_id='cg-50332889')
        self.assertEqual(category.uzh_url,
                         u'http://www.vorlesungen.uzh.ch/fs14/lehrangebot/fak-50000003/sc-50332888/cga-50332888010/cg-50332889.module.html')

    def test_title(self):
        category = Category.objects.get(uzh_id='cg-50332889')
        self.assertEqual(category.title, 'Informatik')

    def test_additional_attributes(self):
        category = Category.objects.get(uzh_id='cg-50014161')
        self.assertEqual(category.study_degree,
                         u'Bachelor of Arts UZH in Wirtschaftswissenschaften Betriebswirtschaftslehre')
        self.assertEqual(category.study_content,
                         u'Der BA vermittelt solides wirtschaftswissenschaftliches Grundlagenwissen und die Fähigkeit zu methodisch- wissenschaftlichem Denken. Studienaufbau (KP = ECTS-Kreditpunkte): Die zweisemestrige Assessmentstufe (60 KP) vermittelt Grundkompetenzen in Betriebs- und Volkswirtschaftslehre, Informatik, Mathematik und Statistik. Der weiterführende Teil des Bachelorstudiums umfasst insgesamt 120 KP, wovon 30 KP im Pflichtbereich (Betriebs- und Volkswirtschaftslehre, Informatik, Statistik) erworben werden. In der Studienrichtung Betriebswirtschaftslehre werden mindestens 36 KP in den Bereichen Accounting, Finanzmanagement, Human Resource Management, Marketing, Unternehmensführung und Operations Research erbracht. Weitere 18 KP werden in der Volkswirtschaftslehre und in Banking and Finance erworben. Zusätzlich können nach Wahl individuelle Schwerpunkte vertieft werden. Die Bachelorarbeit im Umfang von 18 KP rundet das Studium ab.')
        self.assertEqual(category.total_ects, 180)
        self.assertEqual(category.major_minor_regulations,
                         u'Die Studienrichtung Betriebswirtschaftslehre sieht kein Nebenfach vor. Im Rahmen der 18 frei wählbaren ECTS Credits können aber auch Veranstaltungen anderer Fakultäten oder Universitäten belegt werden.')
        self.assertEqual(category.field_of_study, u'Betriebswirtschaftslehre, Finance, Volkswirtschaftslehre')


class TestModuleSpider(TestCase):
    def test_total_amount_of_modules(self):
        modules = Module.objects.all()
        self.assertEqual(len(modules), 26)

    def test_categories(self):
        module = Module.objects.get(uzh_id='sm-50031187')
        categories = module.categories.all()
        self.assertEqual(len(categories), 2)

        module = Module.objects.get(uzh_id='sm-50563704')
        categories = module.categories.all()
        self.assertEqual(len(categories), 1)

    def test_title(self):
        module = Module.objects.get(uzh_id='sm-50031187')
        self.assertEqual(module.title, u'Mobile Communication Syst. (L+E)')

        module = Module.objects.get(uzh_id='sm-50630750')
        self.assertEqual(module.title, u'Autonomous Mobile Robots')

    def test_ects(self):
        module = Module.objects.get(uzh_id='sm-50630750')
        self.assertEqual(module.ects, 4.0)

        module = Module.objects.get(uzh_id='sm-50031187')
        self.assertEqual(module.ects, 4.5)

    def test_persons_in_charge(self):
        module = Module.objects.get(uzh_id='sm-50356543')
        person = module.persons_in_charge.first()
        self.assertEqual(person.firstname, u'Martin')
        self.assertEqual(person.lastname, u'Glinz')
        self.assertEqual(person.title, u'Dr. rer. nat.')

        module = Module.objects.get(uzh_id='sm-50574529')
        person = module.persons_in_charge.first()
        self.assertEqual(person.firstname, u'Abraham')
        self.assertEqual(person.lastname, u'Bernstein')
        self.assertEqual(person.title, u'Dr.')

    def test_uzh_code(self):
        module = Module.objects.get(uzh_id='sm-50662146')
        self.assertEqual(module.uzh_code, 'MINF4538')

        module = Module.objects.get(uzh_id='sm-50357468')
        self.assertEqual(module.uzh_code, 'MINF4224')

    def test_repeatability(self):
        module = Module.objects.get(uzh_id='sm-50019546')
        self.assertEqual(module.repeatability, SimpleAttribute.get_simple_model_object(ModuleRepeatability, u'siehe Reglemente WWF'))

        module = Module.objects.get(uzh_id='sm-50031735')
        self.assertEqual(module.repeatability, SimpleAttribute.get_simple_model_object(ModuleRepeatability, u'einmal wiederholbar'))

    def test_deadlines(self):
        module = Module.objects.get(uzh_id='sm-50525765')
        self.assertEqual(module.booking_deadline, SimpleAttribute.get_simple_model_object(ModuleDeadline, u'von Di 14.01.2014 10:00 bis Fr 14.03.2014 24:00'))
        self.assertEqual(module.cancellation_deadline, SimpleAttribute.get_simple_model_object(ModuleDeadline, u'von Di 14.01.2014 10:00 bis Fr 14.03.2014 24:00'))

        module = Module.objects.get(uzh_id='sm-50626605')
        self.assertEqual(module.booking_deadline, SimpleAttribute.get_simple_model_object(ModuleDeadline, u'von Di 14.01.2014 10:00 bis Fr 14.03.2014 24:00'))
        self.assertEqual(module.cancellation_deadline, SimpleAttribute.get_simple_model_object(ModuleDeadline, u'von Di 14.01.2014 10:00 bis Fr 14.03.2014 24:00'))

    def test_requirements(self):
        module = Module.objects.get(uzh_id='sm-50626605')
        self.assertEqual(module.requirements, 'MSc Informatik: Master-Basismodul')

        module = Module.objects.get(uzh_id='sm-50634071')
        self.assertEqual(module.requirements, u'Students must take the lecture «Computer Graphics» to participate in this lab or have demonstrated prior equivalent knowledge of the fundamental concepts of interactive 3D graphics.')

    def test_language(self):
        module = Module.objects.get(uzh_id='sm-50680312')
        self.assertEqual(module.language, SimpleAttribute.get_simple_model_object(ModuleLanguage, u'English'))

        module = Module.objects.get(uzh_id='sm-50662146')
        self.assertEqual(module.language, SimpleAttribute.get_simple_model_object(ModuleLanguage, u'English'))

        module = Module.objects.get(uzh_id='sm-50352807')
        self.assertEqual(module.language, SimpleAttribute.get_simple_model_object(ModuleLanguage, u'Deutsch'))

    def test_organizational(self):
        module = Module.objects.get(uzh_id='sm-50479030')
        for organizational_unit in module.organizational_units.all():
            self.assertEqual(organizational_unit, SimpleAttribute.get_simple_model_object(ModuleOrganizational, u'Institut für Computerlinguistik'))
        self.assertEqual(module.organizational_units.count(), 1)

        module = Module.objects.get(uzh_id='sm-50390595')
        for organizational_unit in module.organizational_units.all():
            self.assertEqual(organizational_unit, SimpleAttribute.get_simple_model_object(ModuleOrganizational, u'Institut für Informatik'))
        self.assertEqual(module.organizational_units.count(), 1)

    def test_grading_scale(self):
        module = Module.objects.get(uzh_id='sm-50630750')
        self.assertEqual(module.grading_scale, SimpleAttribute.get_simple_model_object(ModuleGradingScale, u'1-6, in Viertelschritten'))

        module = Module.objects.get(uzh_id='sm-50563704')
        self.assertEqual(module.grading_scale, SimpleAttribute.get_simple_model_object(ModuleGradingScale, u'1-6, in Halbschritten'))

    def test_frequency(self):
        module = Module.objects.get(uzh_id='sm-50019546')
        self.assertEqual(module.frequency, SimpleAttribute.get_simple_model_object(ModuleFrequency, u'1-semestrig (jedes 2. Frühjahrssemester)'))

        module = Module.objects.get(uzh_id='sm-50019942')
        self.assertEqual(module.frequency, SimpleAttribute.get_simple_model_object(ModuleFrequency, u'1-semestrig (jedes Frühjahrssemester)'))


class TestLectureSpider(TestCase):
    def test_total_amount_of_lectures(self):
        lectures = Lecture.objects.all()
        self.assertEqual(len(lectures), 3)

    def test_module_relations(self):
        module = Module.objects.get(uzh_id='sm-50574529')
        lectures = module.lectures.all()
        self.assertEqual(len(lectures), 2)

    def test_title(self):
        lecture = Lecture.objects.get(uzh_id='e-50667944')
        self.assertEqual(lecture.title, u'Practical Artificial Intelligence (E)')

    def test_schedules_amount(self):
        lecture = Lecture.objects.get(uzh_id='e-50685434')
        schedules = LectureScheduleItem.objects.filter(lecture=lecture)
        self.assertEqual(len(schedules), 15)

    def test_schedule_items(self):
        current_tz = timezone.get_current_timezone()
        lecture = Lecture.objects.get(uzh_id='e-50685434')
        schedule_item = LectureScheduleItem.objects.filter(lecture=lecture).first()
        start_datetime = schedule_item.start_datetime
        self.assertEqual(start_datetime, current_tz.localize(datetime(2014, 02, 19, 9, 0)))
        end_datetime = schedule_item.end_datetime
        self.assertEqual(end_datetime, current_tz.localize(datetime(2014, 02, 19, 12, 0)))
        room = schedule_item.rooms.first()
        self.assertEqual(room.name, u'BIN-2.A.10')
        self.assertEqual(room.street, u'Binzmühlestrasse 14')
        self.assertEqual(room.address, u'8050 Zürich')
        self.assertEqual(len(schedule_item.lecturers.all()), 2)
        lecturer = schedule_item.lecturers.last()
        self.assertEqual(lecturer.firstname, u'Corinna')
        self.assertEqual(lecturer.lastname, u'Schmitt')
        self.assertEqual(lecturer.title, u'Dr. rer. nat.')

        lecture = Lecture.objects.get(uzh_id='e-50685434')
        schedule_item = LectureScheduleItem.objects.filter(lecture=lecture).last()
        start_datetime = schedule_item.start_datetime
        self.assertEqual(start_datetime, current_tz.localize(datetime(2014, 06, 18, 10, 15)))
        end_datetime = schedule_item.end_datetime
        self.assertEqual(end_datetime, current_tz.localize(datetime(2014, 06, 18, 12, 0)))
        room = schedule_item.rooms.first()
        self.assertEqual(room.name, u'BIN-2.A.01')
        self.assertEqual(room.street, u'Binzmühlestrasse 14')
        self.assertEqual(room.address, u'8050 Zürich')
        self.assertEqual(len(schedule_item.lecturers.all()), 2)
        lecturer = schedule_item.lecturers.first()
        self.assertEqual(lecturer.firstname, u'Burkhard')
        self.assertEqual(lecturer.lastname, u'Stiller')
        self.assertEqual(lecturer.title, u'Dr.')

        lecture = Lecture.objects.get(uzh_id='e-50667942')
        schedule_item = LectureScheduleItem.objects.filter(lecture=lecture).first()
        start_datetime = schedule_item.start_datetime
        self.assertEqual(start_datetime, current_tz.localize(datetime(2014, 02, 18, 14, 0)))
        end_datetime = schedule_item.end_datetime
        self.assertEqual(end_datetime, current_tz.localize(datetime(2014, 02, 18, 15, 45)))
        room = schedule_item.rooms.first()
        self.assertEqual(room.name, u'BIN-2.A.01')
        self.assertEqual(room.street, u'Binzmühlestrasse 14')
        self.assertEqual(room.address, u'8050 Zürich')
        self.assertEqual(len(schedule_item.lecturers.all()), 1)
        lecturer = schedule_item.lecturers.first()
        self.assertEqual(lecturer.firstname, u'Abraham')
        self.assertEqual(lecturer.lastname, u'Bernstein')
        self.assertEqual(lecturer.title, u'Dr.')
