"""
Test cases for keywords extractor
"""
from crawler.feature_extraction.tf_idf import TfIdf

from django.test import TestCase


class TestKeywordsExtractor(TestCase):
    def test_english_text(self):
        text_lists = [
            [
                (u'Computer Graphics (L)', 2),
                (
                    u'''Introduction to the fundamental concepts, algorithms, and data structures of interactive 3D
                computer graphics such as graphics systems, polygonal modeling, illumination and shading, geometric
                transformations, viewing in 3D, visibility, clipping, rasterization, and ray-tracing.''', 1
                ),
                (
                    u'''This course will teach the participating students the fundamental concepts of 3D
                 image synthesis, with a focus on interactive rendering. The students will learn
                 how real-time image formation as used in interactive 3D games, virtual reality
                 simulation and scientific visualization applications is performed through the
                 pipelined processes of perspective projection, visibility determination,
                 illumination, rasterization and shading. A number of techniques to improve
                 realism such as depth-cues, texturing and advanced shading will be discussed.''', 1
                )
            ],
            [
                (u'Mobile Communication Syst. (L+E)', 2),
                (
                    u'''Based on the basic knowledge on communication systems as well as distributed systems
                    the specifics of communications in the wireless and mobile domain are addressed. Those
                    include technical basics, media access schemes, and signals. In a more detailed view
                    mobile and wireless telecommunication systems are discussed, which include GSM, UMTS,
                    satellites, and radio. The Local Area Network is addressed in terms of WLAN technology,
                    bluetooth, and some further examples. The development of these systems into an Internet
                    usage is shown by discussing Mobile IP as well as mobile transport protocols. Finally,
                    mobility support for wireless transactions and file systems as well as deployed
                    technology scenarios are discussed.''', 1),
                (
                    u'''This lecture\'s goals encompass the gaining of knowledge of principles and
                    protocols for wireless and mobile communications. While telecommunications and
                    Internet aspects are of utmost importance, their integration and future needs are
                    discussed. Thus, respective protocol details and technology aspects for mobile and
                     wireless communications will be covered.''', 1)
            ],
            [
                (u'Workshop & Lecture Series in Law & Economics', 2),
                (
                    u'''This workshop and lecture series is a joint project of the ETH Zurich, the University
                    of Zurich, the University of St. Gallen, the University of Lucerne and the University of
                    Basel. It provides an overview of current interdisciplinary research in law and economics.
                    Legal, economics, and psychology scholars give a lecture and/or present their current research.
                    All speakers are internationally well-known experts from Europe, the U.S., and beyond.''', 1),
            ],
            [
                (u'Monetary Macroeconomics (L)', 2),
                (
                    u'''In this course we will build elegantly simple yet rigorous models of money and
                    banking to replicate essential features of actual monetary economies. In an
                    overlapping-generations framework, we will explain aggregate economic phenomena
                    as the implications of the choices of rational individuals who seek to improve
                    their welfare within their limited means. Some of the topics that will be studied are theories
                    of money demand and money supply, inflation, financial intermediation and banking,
                    banking practices and regulations, role of the central bank, and fiscal and monetary
                    policy, among others. A link to real world situations will be provided, but the focus
                    of the course will be theoretical money and banking models with microeconomic foundations.''', 1),
            ],
        ]
        extractor = TfIdf()
        for i, text_list in enumerate(text_lists):
            extractor._add_document_to_corpus(i, text_list)

        results = extractor.extract_keywords(10)
        computer_graphics = results[0]
        self.assertTrue(self._list_contains(computer_graphics, 'computer'))
        self.assertTrue(self._list_contains(computer_graphics, 'graphics'))
        self.assertTrue(self._list_contains(computer_graphics, 'image'))

        mobile_communications = results[1]
        self.assertTrue(self._list_contains(mobile_communications, 'mobile'))
        self.assertTrue(self._list_contains(mobile_communications, 'communication'))
        self.assertTrue(self._list_contains(mobile_communications, 'wireless'))

        law_economics = results[2]
        self.assertTrue(self._list_contains(law_economics, 'law'))
        self.assertTrue(self._list_contains(law_economics, 'economics'))

    def _list_contains(self, list_, keyword):
        for entry in list_:
            if entry[0] == keyword:
                return True
        return False
