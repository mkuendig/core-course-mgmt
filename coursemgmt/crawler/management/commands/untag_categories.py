# coding=utf-8
"""
Remove custom categories from all categories
"""

from crawler.models.simple_attributes import Category

from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    Extract keywords of all modules and saves them in the corresponding module object
    """
    help = 'Tags all categories (custom_category and module_type)'

    def handle(self, **options):
        """
        Crawl the persons for a given semester
        :param options:
        :return:
        """
        categories = Category.objects.all()
        for category in categories:
            category.custom_categories.clear()
            category.save()
