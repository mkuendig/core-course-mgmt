"""
Extract features of all modules and saves them in the corresponding module object
"""
from crawler.feature_extraction.synset_features_extractor import SynsetFeaturesExtractor
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    Extract features of all modules and saves them in the corresponding module object
    """
    help = 'Calculate tf-idf from all processed module descriptions'

    def handle(self, **options):
        """
        Crawl the persons for a given semester
        :param options:
        :return:
        """
        SynsetFeaturesExtractor().calculate_module_keywords()
