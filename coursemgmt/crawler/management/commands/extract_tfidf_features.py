"""
Extract features of all modules and saves them in the corresponding module object
"""
from crawler.feature_extraction.tf_idf import TfIdf
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    Extract features of all modules and saves them in the corresponding module object
    """
    help = 'Calculate tf-idf from all processed module descriptions'

    def handle(self, **options):
        """
        Crawl the persons for a given semester
        :param options:
        :return:
        """
        TfIdf().calculate_module_features()
