from crawler.category_tagger import CategoryTagger
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    Tag all categories according to a self defined category mapping
    """
    help = 'Tags all categories (custom_category and module_type)'

    def handle(self, **options):
        """
        :param options:
        :return:
        """
        CategoryTagger().tag_categories()
