"""
Extract features of all modules and saves them in the corresponding module object
"""
from crawler.feature_extraction.lda_features_extractor import LdaFeaturesExtractor
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    Extract features of all modules and saves them in the corresponding module object
    """
    help = 'Extract features of all modules and saves them in the corresponding module object'

    def handle(self, **options):
        """
        Crawl the persons for a given semester
        :param number_of_topics: float
        :param words_per_topic: float
        :param options:
        :return:
        """
        LdaFeaturesExtractor().extract_lda_features()
