"""
Crawl the modules for a given semester (taxonomy has to be crawled)
"""
from django.core.management import BaseCommand, CommandError
from crawler.core import CrawlerController


class Command(BaseCommand):
    """
    Crawl the modules for a given semester (taxonomy has to be crawled)
    """
    args = '[semester]'
    help = 'Crawl the modules for a given semester (taxonomy has to be crawled)'

    def handle(self, semester_name, **options):
        """
        Crawl the taxonomy for a given semester
        :param semester_name: semester_name according to uzh (e.g fs14 or hs10)
        :param options:
        :return:
        """
        if not semester_name:
            raise CommandError('Semester not specified')
        CrawlerController(semester_name).crawl_modules()
