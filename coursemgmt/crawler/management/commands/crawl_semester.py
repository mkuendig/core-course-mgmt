"""
Crawl the entire course catalog for a given semester
"""
from crawler.core import CrawlerController
from crawler.management.commands.tag_categories import CategoryTagger
from django.core.management import BaseCommand, CommandError


class Command(BaseCommand):
    """
    Crawl the entire course catalog for a given semester
    """
    args = '[semester]'
    help = 'Crawl the entire course catalog for a given semester'

    def handle(self, semester_name, **options):
        """
        Crawl the entire course catalog for a given semester
        :param semester_name: semester_name according to uzh (e.g fs14 or hs10)
        :param options:
        :return:
        """
        if not semester_name:
            raise CommandError('Semester not specified')
        CrawlerController(semester_name).crawl_semester()
        CategoryTagger().tag_categories()
