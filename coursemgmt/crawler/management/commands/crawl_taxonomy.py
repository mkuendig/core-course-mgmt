"""
Crawl the taxonomy for a given semester
"""
from django.core.management import BaseCommand, CommandError
from crawler.core import CrawlerController


class Command(BaseCommand):
    """
    Crawl the whole course catalog for a given semester
    """
    args = '[semester]'
    help = 'Crawl the taxonomy for a given semester'

    def handle(self, semester_name, **options):
        """
        Crawl the taxonomy for a given semester
        :param semester_name: semester_name according to uzh (e.g fs14 or hs10)
        :param options:
        :return:
        """
        if not semester_name:
            raise CommandError('Semester not specified')
        CrawlerController(semester_name).crawl_taxonomy()
