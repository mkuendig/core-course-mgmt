"""
Scrapy Items

The main goal in scraping is to extract structured data from unstructured sources, typically, web pages.
Scrapy provides the Item class for this purpose.

See http://doc.scrapy.org/en/latest/topics/items.html
"""

from scrapy.item import Item, Field


class CategoryItem(Item):
    """
    Srapy item for category
    """
    uzh_id = Field()
    uzh_url = Field()
    title = Field()
    parent_uzh_id = Field()
    level = Field()
    study_degree = Field()
    study_content = Field()
    total_ects = Field()
    major_minor_regulation = Field()
    precognition = Field()
    field_of_study = Field()


class ModuleItem(Item):
    """
    Srapy item for module
    """
    uzh_id = Field()
    uzh_url = Field()
    title = Field()
    uzh_code = Field()
    ects = Field()
    frequency = Field()
    description = Field()
    precognition = Field()
    requirements = Field()
    teaching_materials = Field()
    learning_target = Field()
    target_audience = Field()
    person_items = Field()
    person_ids = Field()
    achievement_test = Field()
    grading_scale = Field()
    repeatability = Field()
    language = Field()
    further_information = Field()
    booking_deadline = Field()
    cancellation_deadline = Field()
    organizational_units = Field()


class LectureItem(Item):
    """
    Srapy item for module
    """
    uzh_id = Field()
    uzh_url = Field()
    title = Field()
    uzh_lecture_id = Field()
    uzh_lecture_code = Field()
    category = Field()
    content = Field()
    teaching_materials = Field()
    schedule = Field()


class PersonItem(Item):
    """
    Srapy item for module
    """
    uzh_id = Field()
    uzh_url = Field()
    title = Field()
    firstname = Field()
    lastname = Field()
    position = Field()


class ScheduleItem(Item):
    """
    Scrapy item for lecture schedule
    """
    date = Field()
    time = Field()
    room_ids = Field()
    room_items = Field()
    lecturer_ids = Field()
    lecturer_items = Field()


class RoomItem(Item):
    """
    Scrappy item for a room
    """
    uzh_id = Field()
    uzh_url = Field()
    name = Field()
    street = Field()
    address = Field()
