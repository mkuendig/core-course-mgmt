"""
Custom  dashboard for crawler admin
"""

from django.utils.translation import ugettext_lazy as _
from grappelli.dashboard import modules, Dashboard


class CrawlerDashboard(Dashboard):
    """
    Custom  dashboard for crawler admin
    """

    def init_with_context(self, context):
        """
        :param context:
        :return:
        """
        # append an app list module for "Applications"
        self.children.append(modules.AppList(
            title=_('course_information'),
            column=1,
            collapsible=True,
            models=('crawler.models.Category', 'crawler.models.Module', 'crawler.models.Lecture')
        ))

        self.children.append(modules.AppList(
            title=_('further_information'),
            column=1,
            collapsible=True,
            models=('crawler.models.Person', 'crawler.models.Room', 'crawler.models.ModuleFeature',
                    'crawler.models.ModuleOrganizational', 'crawler.models.CategoryHierarchyType')
        ))

        # append an app list module for "Administration"
        self.children.append(modules.AppList(
            title=_('administration'),
            column=1,
            collapsible=True,
            models=('django.contrib.auth.*',)
        ))

        self.children.append(modules.AppList(
            title=_('crawl_jobs'),
            column=2,
            collapsible=False,
            models=('crawler.models.CrawlJob',)
        ))

        self.children.append(modules.AppList(
            title=_('export_jobs'),
            column=2,
            collapsible=False,
            models=('crawler.models.ExportJob',)
        ))

        self.children.append(modules.AppList(
            title=_('tag_categories_jobs'),
            column=2,
            collapsible=False,
            models=('crawler.models.TagCategoriesJob',)
        ))
