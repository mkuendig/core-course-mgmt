# coding=utf-8

from crawler.models import Category, CustomCategory, COMPULSORY_ELECTIVE_MODULES, ELECTIVE_MODULES, MANDATORY_MODULES


class CategoryTagger(object):
    def __init__(self):
        CustomCategory.objects.all().delete()
        self._create_hierarchy()

    def tag_categories(self):
        mapping = self._get_mapping()
        self._apply_mapping(mapping)

    def _create_hierarchy(self):
        # Fakultät (level 1)
        self.wwf, _ = CustomCategory.objects.get_or_create(slug='wwf', defaults={
            'title': 'Wirtschaftswissenschaftliche Fakultät',
        })

        # Studiengänge (Level 2)
        self.economics, _ = CustomCategory.objects.get_or_create(slug='economics', defaults={
            'title': 'Wirtschaft',
        })
        self.informatics, _ = CustomCategory.objects.get_or_create(slug='informatics', defaults={
            'title': 'Informatik',
        })

        # Bachelor/Master (Level 3)
        self.bsc_economics, _ = CustomCategory.objects.get_or_create(slug='bsc_economics', defaults={
            'title': 'Bachelor',
        })
        self.bsc_informatics, _ = CustomCategory.objects.get_or_create(slug='bsc_informatics', defaults={
            'title': 'Bachelor',
        })
        self.msc_economics, _ = CustomCategory.objects.get_or_create(slug='msc_economics', defaults={
            'title': 'Master',
        })
        self.msc_informatics, _ = CustomCategory.objects.get_or_create(slug='msc_informatics', defaults={
            'title': 'Master',
        })

        # Hauptfach (Level 4)
        # Bachelor
        self.bwl_bsc_economics, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_economics', defaults={
            'title': 'Betriebswirtschaftslehre',
        })
        self.bf_bsc_economics, _ = CustomCategory.objects.get_or_create(slug='bf_bsc_economics', defaults={
            'title': 'Banking and Finance',
        })
        self.vwl_bsc_economics, _ = CustomCategory.objects.get_or_create(slug='vwl_bsc_economics', defaults={
            'title': 'Volkswirtschaftslehre',
        })
        self.me_bsc_economics, _ = CustomCategory.objects.get_or_create(slug='me_bsc_economics', defaults={
            'title': 'Management and Economics',
        })
        self.wi_bsc_informatics, _ = CustomCategory.objects.get_or_create(slug='wi_bsc_informatics', defaults={
            'title': 'Wirtschaftsinformatik',
        })
        self.ss_bsc_informatics, _ = CustomCategory.objects.get_or_create(slug='ss_bsc_informatics', defaults={
            'title': 'Softwaresysteme',
        })
        self.ani_bsc_informatics, _ = CustomCategory.objects.get_or_create(slug='ani_bsc_informatics', defaults={
            'title': 'Neuroinformatik',
        })
        self.abi_bsc_informatics, _ = CustomCategory.objects.get_or_create(slug='abi_bsc_informatics', defaults={
            'title': 'Bioinformatik',
        })
        self.agi_bsc_informatics, _ = CustomCategory.objects.get_or_create(slug='agi_bsc_informatics', defaults={
            'title': 'Geoinformatik',
        })
        self.ami_bsc_informatics, _ = CustomCategory.objects.get_or_create(slug='ami_bsc_informatics', defaults={
            'title': 'Medieninformatik',
        })
        self.aci_bsc_informatics, _ = CustomCategory.objects.get_or_create(slug='aci_bsc_informatics', defaults={
            'title': 'Computerlinguistik',
        })

        # Master
        self.bwl_msc_economics, _ = CustomCategory.objects.get_or_create(slug='bwl_msc_economics', defaults={
            'title': 'Betriebswirtschaftslehre',
        })
        self.bf_msc_economics, _ = CustomCategory.objects.get_or_create(slug='bf_msc_economics', defaults={
            'title': 'Banking and Finance',
        })
        self.vwl_msc_economics, _ = CustomCategory.objects.get_or_create(slug='vwl_msc_economics', defaults={
            'title': 'Volkswirtschaftslehre',
        })
        self.me_msc_economics, _ = CustomCategory.objects.get_or_create(slug='me_msc_economics', defaults={
            'title': 'Management and Economics',
        })
        self.wi_msc_informatics, _ = CustomCategory.objects.get_or_create(slug='wi_msc_informatics', defaults={
            'title': 'Wirtschaftsinformatik',
        })
        self.ss_msc_informatics, _ = CustomCategory.objects.get_or_create(slug='ss_msc_informatics', defaults={
            'title': 'Softwaresysteme',
        })
        self.cs_msc_informatics, _ = CustomCategory.objects.get_or_create(slug='cs_msc_informatics', defaults={
            'title': 'Multimodale und kognitive Systeme',
        })

        # Modultyp (Level 5)
        # Assessment
        self.bwl_assessment, _ = CustomCategory.objects.get_or_create(slug='bwl_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.vwl_assessment, _ = CustomCategory.objects.get_or_create(slug='vwl_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.bf_assessment, _ = CustomCategory.objects.get_or_create(slug='bf_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.me_assessment, _ = CustomCategory.objects.get_or_create(slug='me_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.wi_assessment, _ = CustomCategory.objects.get_or_create(slug='wi_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.ss_assessment, _ = CustomCategory.objects.get_or_create(slug='ss_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.ani_assessment, _ = CustomCategory.objects.get_or_create(slug='ani_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.abi_assessment, _ = CustomCategory.objects.get_or_create(slug='abi_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.agi_assessment, _ = CustomCategory.objects.get_or_create(slug='agi_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.ami_assessment, _ = CustomCategory.objects.get_or_create(slug='ami_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })
        self.aci_assessment, _ = CustomCategory.objects.get_or_create(slug='aci_assessment', defaults={
            'title': 'Assessment',
            'module_type': MANDATORY_MODULES
        })

        # Mandatory Modules
        # Bachelor
        self.bwl_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_mandatory', defaults={
            'title': 'Pflichtbereich BWL',
            'module_type': MANDATORY_MODULES
        })
        self.vwl_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='vwl_bsc_mandatory', defaults={
            'title': 'Pflichtbereich VWL',
            'module_type': MANDATORY_MODULES
        })
        self.bf_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='bf_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Banking & Finance',
            'module_type': MANDATORY_MODULES
        })
        self.me_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='me_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Management & Economics',
            'module_type': MANDATORY_MODULES
        })

        self.wi_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='wi_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Wirtschaftsinformatik',
            'module_type': MANDATORY_MODULES
        })
        self.ss_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='ss_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Softwaresysteme',
            'module_type': MANDATORY_MODULES
        })
        self.ani_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='ani_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Neuroinformatik',
            'module_type': MANDATORY_MODULES
        })
        self.abi_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='abi_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Bioinformatik',
            'module_type': MANDATORY_MODULES
        })
        self.agi_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='agi_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Geoinformatik',
            'module_type': MANDATORY_MODULES
        })
        self.ami_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='ami_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Medieninformatik',
            'module_type': MANDATORY_MODULES
        })
        self.aci_bsc_mandatory, _ = CustomCategory.objects.get_or_create(slug='aci_bsc_mandatory', defaults={
            'title': 'Pflichtbereich Computerlinguistik',
            'module_type': MANDATORY_MODULES
        })

        # Master
        self.bwl_msc_mandatory, _ = CustomCategory.objects.get_or_create(slug='bwl_msc_mandatory', defaults={
            'title': 'Pflichtbereich BWL',
            'module_type': MANDATORY_MODULES
        })
        self.vwl_msc_mandatory, _ = CustomCategory.objects.get_or_create(slug='vwl_msc_mandatory', defaults={
            'title': 'Pflichtbereich VWL',
            'module_type': MANDATORY_MODULES
        })
        self.bf_msc_mandatory, _ = CustomCategory.objects.get_or_create(slug='bf_msc_mandatory', defaults={
            'title': 'Pflichtbereich Banking & Finance',
            'module_type': MANDATORY_MODULES
        })
        self.me_msc_mandatory, _ = CustomCategory.objects.get_or_create(slug='me_msc_mandatory', defaults={
            'title': 'Pflichtbereich Management & Economics',
            'module_type': MANDATORY_MODULES
        })

        # Compulsory Elective Modules
        # Bachelor
        self.eco_bsc_ce_bwl1, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_bwl1', defaults={
            'title': 'Wahlpflichtbereich Accounting, Auditing and Governance (BWL1)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_bwl2, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_bwl2', defaults={
            'title': 'Wahlpflichtbereich Corporate Finance and Banking (BWL2)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_bwl3, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_bwl3', defaults={
            'title': 'Wahlpflichtbereich Organization and Human Resources (BWL3)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_bwl4, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_bwl4', defaults={
            'title': 'Wahlpflichtbereich Marketing (BWL4)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_bwl5, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_bwl5', defaults={
            'title': 'Wahlpflichtbereich Business Policy and Governance (BWL5)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_bwl6, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_bwl6', defaults={
            'title': 'Wahlpflichtbereich Management Science (BWL6)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_vwl1, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_vwl1', defaults={
            'title': 'Wahlpflichtbereich Makroökonomik (ECON1/VWL1)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_vwl2, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_vwl2', defaults={
            'title': 'Wahlpflichtbereich Mikroökonomik (ECON2/VWL2)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_bf1, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_bf1', defaults={
            'title': 'Pflichtbereich BF 1 (Core Courses...)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_bsc_ce_bf2, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_ce_bf2', defaults={
            'title': 'Wahlpflichtbereich BF 2 (Other Courses)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })

        self.wi_bsc_ce, _ = CustomCategory.objects.get_or_create(slug='wi_bsc_ce', defaults={
            'title': 'Wahlpflichtbereich Wirtschaftsinformatik',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.ss_bsc_ce, _ = CustomCategory.objects.get_or_create(slug='ss_bsc_ce', defaults={
            'title': 'Wahlpflichtbereich Softwaresysteme',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.ani_bsc_ce, _ = CustomCategory.objects.get_or_create(slug='ani_bsc_ce', defaults={
            'title': 'Wahlpflichtbereich Neuroinformatik',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.abi_bsc_ce, _ = CustomCategory.objects.get_or_create(slug='abi_bsc_ce', defaults={
            'title': 'Wahlpflichtbereich Bioinformatik',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.agi_bsc_ce, _ = CustomCategory.objects.get_or_create(slug='agi_bsc_ce', defaults={
            'title': 'Wahlpflichtbereich Geoinformatik',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.ami_bsc_ce, _ = CustomCategory.objects.get_or_create(slug='ami_bsc_ce', defaults={
            'title': 'Wahlpflichtbereich Medieninformatik',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.aci_bsc_ce, _ = CustomCategory.objects.get_or_create(slug='aci_bsc_ce', defaults={
            'title': 'Wahlpflichtbereich Computerlinguistik',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })

        # Master
        self.eco_msc_ce_bwl1, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_bwl1', defaults={
            'title': 'Wahlpflichtbereich Accounting, Auditing and Governance (BWL1)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_msc_ce_bwl2, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_bwl2', defaults={
            'title': 'Wahlpflichtbereich Corporate Finance and Banking (BWL2)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_msc_ce_bwl3, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_bwl3', defaults={
            'title': 'Wahlpflichtbereich Organization and Human Resources (BWL3)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_msc_ce_bwl4, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_bwl4', defaults={
            'title': 'Wahlpflichtbereich Marketing (BWL4)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_msc_ce_bwl5, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_bwl5', defaults={
            'title': 'Wahlpflichtbereich Business Policy and Governance (BWL5)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_msc_ce_bwl6, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_bwl6', defaults={
            'title': 'Wahlpflichtbereich Management Science (BWL6)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_msc_ce_vwl1, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_vwl1', defaults={
            'title': 'Wahlpflichtbereich Makroökonomik (ECON1/VWL1)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_msc_ce_vwl2, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_vwl2', defaults={
            'title': 'Wahlpflichtbereich Mikroökonomik (ECON2/VWL2)',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.eco_msc_ce_vwl3, _ = CustomCategory.objects.get_or_create(slug='eco_msc_ce_vwl3', defaults={
            'title': 'WP ECON3/VWL3',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.bf_msc_ce_bf, _ = CustomCategory.objects.get_or_create(slug='bf_msc_ce_bf', defaults={
            'title': 'Wahlpflichtbereich Banking & Finance',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.me_msc_ce_me1, _ = CustomCategory.objects.get_or_create(slug='me_msc_ce_me1', defaults={
            'title': 'Seminare Management and Economics',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.me_msc_ce_me2, _ = CustomCategory.objects.get_or_create(slug='me_msc_ce_me2', defaults={
            'title': 'Wahlpflichtbereich Empirie',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })

        self.wi_msc_ce, _ = CustomCategory.objects.get_or_create(slug='wi_msc_ce', defaults={
            'title': 'Wahlpflichtbereich Wirtschaftsinformatik',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.wi_msc_se, _ = CustomCategory.objects.get_or_create(slug='wi_msc_se', defaults={
            'title': 'Seminare Wirtschaftsinformatik',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.ss_msc_ce, _ = CustomCategory.objects.get_or_create(slug='ss_msc_ce', defaults={
            'title': 'Wahlpflichtbereich Softwaresysteme',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.ss_msc_se, _ = CustomCategory.objects.get_or_create(slug='ss_msc_se', defaults={
            'title': 'Seminare Softwaresysteme',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.cs_msc_ce, _ = CustomCategory.objects.get_or_create(slug='cs_msc_ce', defaults={
            'title': 'Wahlpflichtbereich Multimodale und kognitive Systeme',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })
        self.cs_msc_se, _ = CustomCategory.objects.get_or_create(slug='cs_msc_se', defaults={
            'title': 'Seminare Multimodale und kognitive Systeme',
            'module_type': COMPULSORY_ELECTIVE_MODULES
        })

        # Elective Modules
        # Bachelor
        self.bwl_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='bwl_bsc_elective', defaults={
            'title': 'Wahlbereich BWL',
            'module_type': ELECTIVE_MODULES
        })
        self.vwl_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='vwl_bsc_elective', defaults={
            'title': 'Wahlbereich VWL',
            'module_type': ELECTIVE_MODULES
        })
        self.bf_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='bf_bsc_elective', defaults={
            'title': 'Wahlbereich BF',
            'module_type': ELECTIVE_MODULES
        })
        self.me_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='me_bsc_elective', defaults={
            'title': 'Wahlbereich ME',
            'module_type': ELECTIVE_MODULES
        })

        self.wi_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='wi_bsc_elective', defaults={
            'title': 'Wahlbereich Wirtschaftsinformatik',
            'module_type': ELECTIVE_MODULES
        })
        self.ss_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='ss_bsc_elective', defaults={
            'title': 'Wahlbereich Softwaresysteme',
            'module_type': ELECTIVE_MODULES
        })
        self.ani_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='ani_bsc_elective', defaults={
            'title': 'Wahlbereich Neuroinformatik',
            'module_type': ELECTIVE_MODULES
        })
        self.abi_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='abi_bsc_elective', defaults={
            'title': 'Wahlbereich Bioinformatik',
            'module_type': ELECTIVE_MODULES
        })
        self.agi_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='agi_bsc_elective', defaults={
            'title': 'Wahlbereich Geoinformatik',
            'module_type': ELECTIVE_MODULES
        })
        self.ami_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='ami_bsc_elective', defaults={
            'title': 'Wahlbereich Medieninformatik',
            'module_type': ELECTIVE_MODULES
        })
        self.aci_bsc_elective, _ = CustomCategory.objects.get_or_create(slug='aci_bsc_elective', defaults={
            'title': 'Wahlbereich Computerlinguistik',
            'module_type': ELECTIVE_MODULES
        })

        # Master
        self.bwl_msc_elective, _ = CustomCategory.objects.get_or_create(slug='bwl_msc_elective', defaults={
            'title': 'Wahlbereich BWL',
            'module_type': ELECTIVE_MODULES
        })
        self.vwl_msc_elective, _ = CustomCategory.objects.get_or_create(slug='vwl_msc_elective', defaults={
            'title': 'Wahlbereich VWL',
            'module_type': ELECTIVE_MODULES
        })
        self.bf_msc_elective, _ = CustomCategory.objects.get_or_create(slug='bf_msc_elective', defaults={
            'title': 'Wahlbereich Banking & Finance',
            'module_type': ELECTIVE_MODULES
        })

        self.wi_msc_elective, _ = CustomCategory.objects.get_or_create(slug='wi_msc_elective', defaults={
            'title': 'Wahlbereich Wirtschaftsinformatik',
            'module_type': ELECTIVE_MODULES
        })
        self.ss_msc_elective, _ = CustomCategory.objects.get_or_create(slug='ss_msc_elective', defaults={
            'title': 'Wahlbereich Softwaresysteme',
            'module_type': ELECTIVE_MODULES
        })
        self.cs_msc_elective, _ = CustomCategory.objects.get_or_create(slug='cs_msc_elective', defaults={
            'title': 'Wahlbereich Multimodale und kognitive Systeme',
            'module_type': ELECTIVE_MODULES
        })

        # Create Hierarchy

        # Level 2
        self.economics.parents.add(self.wwf)
        self.informatics.parents.add(self.wwf)

        # Level 3
        self.bsc_economics.parents.add(self.economics)
        self.bsc_informatics.parents.add(self.informatics)

        self.msc_economics.parents.add(self.economics)
        self.msc_informatics.parents.add(self.informatics)

        # Level 4
        # Bachelor
        self.bwl_bsc_economics.parents.add(self.bsc_economics)
        self.bf_bsc_economics.parents.add(self.bsc_economics)
        self.vwl_bsc_economics.parents.add(self.bsc_economics)
        self.me_bsc_economics.parents.add(self.bsc_economics)
        self.wi_bsc_informatics.parents.add(self.bsc_informatics)
        self.ss_bsc_informatics.parents.add(self.bsc_informatics)
        self.ani_bsc_informatics.parents.add(self.bsc_informatics)
        self.abi_bsc_informatics.parents.add(self.bsc_informatics)
        self.agi_bsc_informatics.parents.add(self.bsc_informatics)
        self.ami_bsc_informatics.parents.add(self.bsc_informatics)
        self.aci_bsc_informatics.parents.add(self.bsc_informatics)

        # Master
        self.bwl_msc_economics.parents.add(self.msc_economics)
        self.bf_msc_economics.parents.add(self.msc_economics)
        self.vwl_msc_economics.parents.add(self.msc_economics)
        self.me_msc_economics.parents.add(self.msc_economics)
        self.wi_msc_informatics.parents.add(self.msc_informatics)
        self.ss_msc_informatics.parents.add(self.msc_informatics)
        self.cs_msc_informatics.parents.add(self.msc_informatics)

        # Level 5
        # Bachelor BWL
        self.bwl_assessment.parents.add(self.bwl_bsc_economics)
        self.bwl_bsc_mandatory.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bwl1.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bwl1.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bwl2.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bwl3.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bwl4.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bwl5.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bwl6.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_vwl1.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_vwl2.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bf1.parents.add(self.bwl_bsc_economics)
        self.eco_bsc_ce_bf2.parents.add(self.bwl_bsc_economics)
        self.bwl_bsc_elective.parents.add(self.bwl_bsc_economics)

        # Bachelor BF
        self.bf_assessment.parents.add(self.bf_bsc_economics)
        self.bf_bsc_mandatory.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_bwl1.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_bwl1.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_bwl2.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_bwl3.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_bwl4.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_bwl5.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_bwl6.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_vwl1.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_vwl2.parents.add(self.bf_bsc_economics)
        self.eco_bsc_ce_bf2.parents.add(self.bf_bsc_economics)
        self.bf_bsc_elective.parents.add(self.bf_bsc_economics)

        # Bachelor VWL
        self.vwl_assessment.parents.add(self.vwl_bsc_economics)
        self.bwl_bsc_mandatory.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bwl1.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bwl1.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bwl2.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bwl3.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bwl4.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bwl5.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bwl6.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_vwl1.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_vwl2.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bf1.parents.add(self.vwl_bsc_economics)
        self.eco_bsc_ce_bf2.parents.add(self.vwl_bsc_economics)
        self.bwl_bsc_elective.parents.add(self.vwl_bsc_economics)

        # Bachelor ME
        self.me_assessment.parents.add(self.me_bsc_economics)
        self.me_bsc_mandatory.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bwl1.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bwl1.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bwl2.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bwl3.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bwl4.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bwl5.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bwl6.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_vwl1.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_vwl2.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bf1.parents.add(self.me_bsc_economics)
        self.eco_bsc_ce_bf2.parents.add(self.me_bsc_economics)
        self.me_bsc_elective.parents.add(self.me_bsc_economics)

        # Bachelor WI
        self.wi_assessment.parents.add(self.wi_bsc_informatics)
        self.wi_bsc_mandatory.parents.add(self.wi_bsc_informatics)
        self.wi_bsc_ce.parents.add(self.wi_bsc_informatics)
        self.wi_bsc_elective.parents.add(self.wi_bsc_informatics)

        # Bachelor SS
        self.ss_assessment.parents.add(self.ss_bsc_informatics)
        self.ss_bsc_mandatory.parents.add(self.ss_bsc_informatics)
        self.ss_bsc_ce.parents.add(self.ss_bsc_informatics)
        self.ss_bsc_elective.parents.add(self.ss_bsc_informatics)

        # Bachelor ANI
        self.ani_assessment.parents.add(self.ani_bsc_informatics)
        self.ani_bsc_mandatory.parents.add(self.ani_bsc_informatics)
        self.ani_bsc_ce.parents.add(self.ani_bsc_informatics)
        self.ani_bsc_elective.parents.add(self.ani_bsc_informatics)

        # Bachelor ABI
        self.abi_assessment.parents.add(self.abi_bsc_informatics)
        self.abi_bsc_mandatory.parents.add(self.abi_bsc_informatics)
        self.abi_bsc_ce.parents.add(self.abi_bsc_informatics)
        self.abi_bsc_elective.parents.add(self.abi_bsc_informatics)

        # Bachelor AGI
        self.agi_assessment.parents.add(self.agi_bsc_informatics)
        self.agi_bsc_mandatory.parents.add(self.agi_bsc_informatics)
        self.agi_bsc_ce.parents.add(self.agi_bsc_informatics)
        self.agi_bsc_elective.parents.add(self.agi_bsc_informatics)

        # Bachelor AMI
        self.ami_assessment.parents.add(self.ami_bsc_informatics)
        self.ami_bsc_mandatory.parents.add(self.ami_bsc_informatics)
        self.ami_bsc_ce.parents.add(self.ami_bsc_informatics)
        self.ami_bsc_elective.parents.add(self.ami_bsc_informatics)

        # Bachelor ACI
        self.aci_assessment.parents.add(self.aci_bsc_informatics)
        self.aci_bsc_mandatory.parents.add(self.aci_bsc_informatics)
        self.aci_bsc_ce.parents.add(self.aci_bsc_informatics)
        self.aci_bsc_elective.parents.add(self.aci_bsc_informatics)

        # Master BWL
        self.bwl_msc_mandatory.parents.add(self.bwl_msc_economics)
        self.eco_msc_ce_bwl1.parents.add(self.bwl_msc_economics)
        self.eco_msc_ce_bwl2.parents.add(self.bwl_msc_economics)
        self.eco_msc_ce_bwl3.parents.add(self.bwl_msc_economics)
        self.eco_msc_ce_bwl4.parents.add(self.bwl_msc_economics)
        self.eco_msc_ce_bwl5.parents.add(self.bwl_msc_economics)
        self.eco_msc_ce_bwl6.parents.add(self.bwl_msc_economics)
        self.bwl_msc_elective.parents.add(self.bwl_msc_economics)

        # Master BF
        self.bf_msc_mandatory.parents.add(self.bf_msc_economics)
        self.bf_msc_ce_bf.parents.add(self.bf_msc_economics)
        self.eco_msc_ce_bwl1.parents.add(self.bf_msc_economics)
        self.eco_msc_ce_bwl2.parents.add(self.bf_msc_economics)
        self.eco_msc_ce_bwl3.parents.add(self.bf_msc_economics)
        self.eco_msc_ce_bwl4.parents.add(self.bf_msc_economics)
        self.eco_msc_ce_bwl5.parents.add(self.bf_msc_economics)
        self.eco_msc_ce_bwl6.parents.add(self.bf_msc_economics)
        self.eco_msc_ce_vwl1.parents.add(self.bf_msc_economics)
        self.eco_msc_ce_vwl2.parents.add(self.bf_msc_economics)
        self.bf_msc_elective.parents.add(self.bf_msc_economics)

        # Master VWL
        self.vwl_msc_mandatory.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_vwl1.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_vwl2.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_vwl3.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_bwl1.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_bwl2.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_bwl3.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_bwl4.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_bwl5.parents.add(self.vwl_msc_economics)
        self.eco_msc_ce_bwl6.parents.add(self.vwl_msc_economics)
        self.vwl_msc_elective.parents.add(self.vwl_msc_economics)

        # Master ME
        self.me_msc_mandatory.parents.add(self.me_msc_economics)
        self.me_msc_ce_me1.parents.add(self.me_msc_economics)
        self.me_msc_ce_me2.parents.add(self.me_msc_economics)
        self.eco_msc_ce_bwl1.parents.add(self.me_msc_economics)
        self.eco_msc_ce_bwl2.parents.add(self.me_msc_economics)
        self.eco_msc_ce_bwl3.parents.add(self.me_msc_economics)
        self.eco_msc_ce_bwl4.parents.add(self.me_msc_economics)
        self.eco_msc_ce_bwl5.parents.add(self.me_msc_economics)
        self.eco_msc_ce_bwl6.parents.add(self.me_msc_economics)
        self.eco_msc_ce_vwl2.parents.add(self.me_msc_economics)

        # Master WI
        self.wi_msc_ce.parents.add(self.wi_msc_informatics)
        self.wi_msc_se.parents.add(self.wi_msc_informatics)
        self.wi_msc_elective.parents.add(self.wi_msc_informatics)

        # Master SS
        self.ss_msc_ce.parents.add(self.ss_msc_informatics)
        self.ss_msc_se.parents.add(self.ss_msc_informatics)
        self.ss_msc_elective.parents.add(self.ss_msc_informatics)

        # Master CS
        self.cs_msc_ce.parents.add(self.cs_msc_informatics)
        self.cs_msc_se.parents.add(self.cs_msc_informatics)
        self.cs_msc_elective.parents.add(self.cs_msc_informatics)

    def _get_mapping(self):
        return [
            # Fakultät
            ('fak-50000003', {'custom_category': self.wwf}),

            # Studiengänge Wirtschaft
            # RO04 (Bsc)
            ('sc-50014157', {'custom_category': self.economics}),
            # RO06 (Msc)
            ('sc-50352369', {'custom_category': self.economics}),
            # RVO16 (Bsc & Msc)
            ('sc-50772013', {'custom_category': self.economics}),
            ('sc-50772686', {'custom_category': self.economics}),

            # Studiengänge Informatik
            # RO08 (Bsc)
            ('sc-50427948', {'custom_category': self.informatics}),
            # RO06 (Msc)
            ('sc-50332888', {'custom_category': self.informatics}),
            # RVO16 (Bsc & Msc)
            ('sc-50772079', {'custom_category': self.informatics}),
            ('sc-50773260', {'custom_category': self.informatics}),

            # Bachelor / Master Stufen
            # RO04 (Bsc)
            ('cga-50014157010', {'custom_category': self.bsc_economics}),
            # RVO16 (Bsc)
            ('cga-50772013380', {'custom_category': self.bsc_economics}),
            ('cga-50772013160', {'custom_category': self.bsc_economics}),
            ('cga-50772013161', {'custom_category': self.bsc_economics}),
            # RO06 (Msc)
            ('cga-50352369010', {'custom_category': self.msc_economics}),
            # RVO16 (Msc)
            ('cga-50772686250', {'custom_category': self.msc_economics}),
            ('cga-50772686190', {'custom_category': self.msc_economics}),

            # RO08 (Bsc)
            ('cga-50427948010', {'custom_category': self.bsc_informatics}),
            # RVO16 (Bsc)
            ('cga-50772079380', {'custom_category': self.bsc_informatics}),
            ('cga-50772079140', {'custom_category': self.bsc_informatics}),
            ('cga-50772079151', {'custom_category': self.bsc_informatics}),
            ('cga-50772079160', {'custom_category': self.bsc_informatics}),
            ('cga-50772079161', {'custom_category': self.bsc_informatics}),
            # RO06 (Msc)
            ('cga-50332888010', {'custom_category': self.msc_informatics}),
            # RVO16 (Msc)
            ('cga-50773260250', {'custom_category': self.msc_informatics}),
            ('cga-50773260190', {'custom_category': self.msc_informatics}),
            ('cga-50773260191', {'custom_category': self.msc_informatics}),

            # Bachelor Wirtschaftswissenschaften
            # ('cg-50014159', {'custom_category': self.assessment}),
            # ('cg-50772067', {'custom_category': self.assessment}),

            ('cg-50014161', {'custom_category': self.bwl_bsc_economics}),
            ('cg-50772017', {'custom_category': self.bwl_bsc_economics}),
            ('cg-50014159', {'custom_category': self.bwl_assessment}),
            ('cg-50772067', {'custom_category': self.bwl_assessment}),
            ('cg-50014167', {'custom_category': self.bwl_bsc_mandatory}),
            ('cg-50772069', {'custom_category': self.bwl_bsc_mandatory}),
            ('cg-50014172', {'custom_category': self.eco_bsc_ce_bwl1}),
            ('cg-50014173', {'custom_category': self.eco_bsc_ce_bwl2}),
            ('cg-50014174', {'custom_category': self.eco_bsc_ce_bwl3}),
            ('cg-50014175', {'custom_category': self.eco_bsc_ce_bwl4}),
            ('cg-50014176', {'custom_category': self.eco_bsc_ce_bwl5}),
            ('cg-50014177', {'custom_category': self.eco_bsc_ce_bwl6}),
            ('cg-50014168', {'custom_category': self.eco_bsc_ce_vwl1}),
            ('cg-50014169', {'custom_category': self.eco_bsc_ce_vwl2}),
            ('cg-50014178', {'custom_category': self.eco_bsc_ce_bf1}),
            ('cg-50014179', {'custom_category': self.eco_bsc_ce_bf2}),
            ('cg-50014189', {'custom_category': self.bwl_bsc_elective}),

            ('cg-50014160', {'custom_category': self.vwl_bsc_economics}),
            ('cg-50772016', {'custom_category': self.vwl_bsc_economics}),
            ('cg-50014159', {'custom_category': self.vwl_assessment}),
            ('cg-50772067', {'custom_category': self.vwl_assessment}),
            ('cg-50014167', {'custom_category': self.vwl_bsc_mandatory}),
            ('cg-50772069', {'custom_category': self.vwl_bsc_mandatory}),
            ('cg-50014168', {'custom_category': self.eco_bsc_ce_vwl1}),
            ('cg-50014169', {'custom_category': self.eco_bsc_ce_vwl2}),
            ('cg-50014178', {'custom_category': self.eco_bsc_ce_bf1}),
            ('cg-50014179', {'custom_category': self.eco_bsc_ce_bf2}),
            ('cg-50014172', {'custom_category': self.eco_bsc_ce_bwl1}),
            ('cg-50014173', {'custom_category': self.eco_bsc_ce_bwl2}),
            ('cg-50014174', {'custom_category': self.eco_bsc_ce_bwl3}),
            ('cg-50014175', {'custom_category': self.eco_bsc_ce_bwl4}),
            ('cg-50014176', {'custom_category': self.eco_bsc_ce_bwl5}),
            ('cg-50014177', {'custom_category': self.eco_bsc_ce_bwl6}),
            ('cg-50014189', {'custom_category': self.vwl_bsc_elective}),
            ('cg-50014170', {'custom_category': self.vwl_bsc_elective}),

            ('cg-50014181', {'custom_category': self.bf_bsc_economics}),
            ('cg-50014159', {'custom_category': self.bf_assessment}),
            ('cg-50772067', {'custom_category': self.bf_assessment}),
            ('cg-50014167', {'custom_category': self.bf_bsc_mandatory}),
            ('cg-50014178', {'custom_category': self.bf_bsc_mandatory}),
            ('cg-50772069', {'custom_category': self.bf_bsc_mandatory}),
            ('cg-50772744', {'custom_category': self.bf_bsc_mandatory}),
            ('cg-50772341', {'custom_category': self.eco_bsc_ce_bf1}),
            ('cg-50014179', {'custom_category': self.eco_bsc_ce_bf2}),
            ('cg-50014168', {'custom_category': self.eco_bsc_ce_vwl1}),
            ('cg-50014169', {'custom_category': self.eco_bsc_ce_vwl2}),
            ('cg-50014172', {'custom_category': self.eco_bsc_ce_bwl1}),
            ('cg-50014173', {'custom_category': self.eco_bsc_ce_bwl2}),
            ('cg-50014174', {'custom_category': self.eco_bsc_ce_bwl3}),
            ('cg-50014175', {'custom_category': self.eco_bsc_ce_bwl4}),
            ('cg-50014176', {'custom_category': self.eco_bsc_ce_bwl5}),
            ('cg-50014177', {'custom_category': self.eco_bsc_ce_bwl6}),
            ('cg-50014189', {'custom_category': self.bf_bsc_elective}),

            ('cg-50014183', {'custom_category': self.me_bsc_economics}),
            ('cg-50014159', {'custom_category': self.me_assessment}),
            ('cg-50772067', {'custom_category': self.me_assessment}),
            ('cg-50014167', {'custom_category': self.me_bsc_mandatory}),
            ('cg-50014171', {'custom_category': self.me_bsc_mandatory}),
            ('cg-50014178', {'custom_category': self.eco_bsc_ce_bf1}),
            ('cg-50014179', {'custom_category': self.eco_bsc_ce_bf2}),
            ('cg-50014168', {'custom_category': self.eco_bsc_ce_vwl1}),
            ('cg-50014169', {'custom_category': self.eco_bsc_ce_vwl2}),
            ('cg-50014172', {'custom_category': self.eco_bsc_ce_bwl1}),
            ('cg-50014173', {'custom_category': self.eco_bsc_ce_bwl2}),
            ('cg-50014174', {'custom_category': self.eco_bsc_ce_bwl3}),
            ('cg-50014175', {'custom_category': self.eco_bsc_ce_bwl4}),
            ('cg-50014176', {'custom_category': self.eco_bsc_ce_bwl5}),
            ('cg-50014177', {'custom_category': self.eco_bsc_ce_bwl6}),
            ('cg-50014189', {'custom_category': self.me_bsc_elective}),

            # Bachelor Informatik

            ('cg-50427949', {'custom_category': self.wi_bsc_informatics}),
            ('cg-50427953', {'custom_category': self.wi_assessment}),
            ('cg-50428039', {'custom_category': self.wi_bsc_mandatory}),
            ('cg-50428044', {'custom_category': self.wi_bsc_ce}),
            ('cg-50428049', {'custom_category': self.wi_bsc_elective}),

            ('cg-50427950', {'custom_category': self.ss_bsc_informatics}),
            ('cg-50427953', {'custom_category': self.ss_assessment}),
            ('cg-50428052', {'custom_category': self.ss_bsc_mandatory}),
            ('cg-50428313', {'custom_category': self.ss_bsc_ce}),
            ('cg-50428049', {'custom_category': self.ss_bsc_elective}),

            ('cg-50428055', {'custom_category': self.ani_bsc_informatics}),
            ('cg-50428161', {'custom_category': self.ani_assessment}),
            ('cg-50480984', {'custom_category': self.ani_bsc_mandatory}),
            ('cg-50428175', {'custom_category': self.ani_bsc_ce}),
            ('cg-50428176', {'custom_category': self.ani_bsc_elective}),

            ('cg-50428057', {'custom_category': self.abi_bsc_informatics}),
            ('cg-50428183', {'custom_category': self.abi_assessment}),
            ('cg-50480985', {'custom_category': self.abi_bsc_mandatory}),
            ('cg-50428175', {'custom_category': self.abi_bsc_ce}),
            ('cg-50428317', {'custom_category': self.abi_bsc_elective}),

            ('cg-50428058', {'custom_category': self.agi_bsc_informatics}),
            ('cg-50428321', {'custom_category': self.agi_assessment}),
            ('cg-50480988', {'custom_category': self.agi_bsc_mandatory}),
            ('cg-50428175', {'custom_category': self.agi_bsc_ce}),
            ('cg-50428326', {'custom_category': self.agi_bsc_elective}),

            ('cg-50475410', {'custom_category': self.ami_bsc_informatics}),
            ('cg-50475411', {'custom_category': self.ami_assessment}),
            ('cg-50428174', {'custom_category': self.ami_bsc_mandatory}),
            ('cg-50428175', {'custom_category': self.ami_bsc_ce}),
            ('cg-50475414', {'custom_category': self.ami_bsc_elective}),

            ('cg-50428159', {'custom_category': self.aci_bsc_informatics}),
            ('cg-50428328', {'custom_category': self.aci_assessment}),
            ('cg-50480990', {'custom_category': self.aci_bsc_mandatory}),
            ('cg-50428175', {'custom_category': self.aci_bsc_ce}),
            ('cg-50428331', {'custom_category': self.aci_bsc_elective}),

            # Master Wirtschaftswissenschaften
            ('cg-50352371', {'custom_category': self.bwl_msc_economics}),
            ('cg-50352378', {'custom_category': self.bwl_msc_mandatory}),
            ('cg-50352379', {'custom_category': self.eco_msc_ce_bwl1}),
            ('cg-50352381', {'custom_category': self.eco_msc_ce_bwl2}),
            ('cg-50352410', {'custom_category': self.eco_msc_ce_bwl3}),
            ('cg-50352411', {'custom_category': self.eco_msc_ce_bwl4}),
            ('cg-50352412', {'custom_category': self.eco_msc_ce_bwl5}),
            ('cg-50352413', {'custom_category': self.eco_msc_ce_bwl6}),
            ('cg-50352416', {'custom_category': self.bwl_msc_elective}),

            ('cg-50352375', {'custom_category': self.bf_msc_economics}),
            ('cg-50352424', {'custom_category': self.bf_msc_mandatory}),
            ('cg-50352427', {'custom_category': self.bf_msc_ce_bf}),
            ('cg-50352379', {'custom_category': self.eco_msc_ce_bwl1}),
            ('cg-50352381', {'custom_category': self.eco_msc_ce_bwl2}),
            ('cg-50352410', {'custom_category': self.eco_msc_ce_bwl3}),
            ('cg-50352411', {'custom_category': self.eco_msc_ce_bwl4}),
            ('cg-50352412', {'custom_category': self.eco_msc_ce_bwl5}),
            ('cg-50352413', {'custom_category': self.eco_msc_ce_bwl6}),
            ('cg-50352419', {'custom_category': self.eco_msc_ce_vwl1}),
            ('cg-50352421', {'custom_category': self.eco_msc_ce_vwl2}),
            ('cg-50352416', {'custom_category': self.bf_msc_elective}),

            ('cg-50352376', {'custom_category': self.me_msc_economics}),
            ('cg-50352429', {'custom_category': self.me_msc_mandatory}),
            ('cg-50387882', {'custom_category': self.me_msc_ce_me1}),
            ('cg-50387549', {'custom_category': self.me_msc_ce_me2}),
            ('cg-50352379', {'custom_category': self.eco_msc_ce_bwl1}),
            ('cg-50352381', {'custom_category': self.eco_msc_ce_bwl2}),
            ('cg-50352410', {'custom_category': self.eco_msc_ce_bwl3}),
            ('cg-50352411', {'custom_category': self.eco_msc_ce_bwl4}),
            ('cg-50352412', {'custom_category': self.eco_msc_ce_bwl5}),
            ('cg-50352413', {'custom_category': self.eco_msc_ce_bwl6}),
            ('cg-50352421', {'custom_category': self.eco_msc_ce_vwl2}),

            ('cg-50352373', {'custom_category': self.vwl_msc_economics}),
            ('cg-50352417', {'custom_category': self.vwl_msc_mandatory}),
            ('cg-50352419', {'custom_category': self.eco_msc_ce_vwl1}),
            ('cg-50352421', {'custom_category': self.eco_msc_ce_vwl2}),
            ('cg-50352423', {'custom_category': self.eco_msc_ce_vwl3}),
            ('cg-50352379', {'custom_category': self.eco_msc_ce_bwl1}),
            ('cg-50352381', {'custom_category': self.eco_msc_ce_bwl2}),
            ('cg-50352410', {'custom_category': self.eco_msc_ce_bwl3}),
            ('cg-50352411', {'custom_category': self.eco_msc_ce_bwl4}),
            ('cg-50352412', {'custom_category': self.eco_msc_ce_bwl5}),
            ('cg-50352413', {'custom_category': self.eco_msc_ce_bwl6}),
            ('cg-50352416', {'custom_category': self.vwl_msc_elective}),

            # Master Informatik
            ('cg-50332890', {'custom_category': self.wi_msc_informatics}),
            ('cg-50332892', {'custom_category': self.ss_msc_informatics}),
            ('cg-50332893', {'custom_category': self.cs_msc_informatics}),
            ('cg-50463945', {'custom_category': self.wi_msc_informatics}),  # fast track
            ('cg-50463947', {'custom_category': self.ss_msc_informatics}),  # fast track
            ('cg-50463948', {'custom_category': self.cs_msc_informatics}),  # fast track

            ('cg-50350083', {'custom_category': self.wi_msc_ce}),
            ('cg-50350159', {'custom_category': self.wi_msc_se}),
            ('cg-50332970', {'custom_category': self.wi_msc_elective}),
            ('cg-50350080', {'custom_category': self.ss_msc_ce}),
            ('cg-50350159', {'custom_category': self.ss_msc_se}),
            ('cg-50332970', {'custom_category': self.ss_msc_elective}),
            ('cg-50332907', {'custom_category': self.cs_msc_ce}),
            ('cg-50350159', {'custom_category': self.cs_msc_se}),
            ('cg-50332970', {'custom_category': self.cs_msc_elective}),

        ]

    @staticmethod
    def _fix_assessment_category_hierarchy():
        assessment_categories = Category.objects.filter(uzh_id="cg-50014159")
        bwl_categories = list(Category.objects.filter(uzh_id="cg-50014161"))
        vwl_categories = list(Category.objects.filter(uzh_id="cg-50014160"))
        bf_categories = list(Category.objects.filter(uzh_id="cg-50014181"))
        me_categories = list(Category.objects.filter(uzh_id="cg-50014183"))

        for assessment_category in assessment_categories:
            assessment_category.parents.add(*bwl_categories)
            assessment_category.parents.add(*vwl_categories)
            assessment_category.parents.add(*bf_categories)
            assessment_category.parents.add(*me_categories)
            assessment_category.save()

    def _apply_mapping(self, mapping):
        self._fix_assessment_category_hierarchy()
        for entry in mapping:
            uzh_id = entry[0]
            updates = entry[1]
            categories = Category.objects.filter(uzh_id=uzh_id)
            if 'custom_category' in updates:
                for category in categories:
                    category.custom_categories.add(updates['custom_category'])
            category.save()
