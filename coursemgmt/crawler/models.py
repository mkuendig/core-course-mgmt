"""
Models reflecting all information available on http://www.vorlesungen.uzh.ch/
"""
import thread
import reversion
from django.core.management import call_command
from django.db import models
from django.db import transaction
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

CRAWL_TAXONOMY = 'crawl_taxonomy'
CRAWL_SEMESTER = 'crawl_semester'
CRAWL_MODULES = 'crawl_modules'
CRAWL_LECTURES = 'crawl_lectures'

EXPORT_TO_METEOR = 'export_to_meteor'
TAG_CATEGROIES = 'tag_categories'

CRAWL_JOBS_CHOICES = (
    (CRAWL_SEMESTER, _(CRAWL_SEMESTER)),
    (CRAWL_TAXONOMY, _(CRAWL_TAXONOMY)),
    (CRAWL_MODULES, _(CRAWL_MODULES)),
    (CRAWL_LECTURES, _(CRAWL_LECTURES))
)


class CrawlJob(models.Model):
    """
    All crawls are organized into jobs
    """
    command = models.CharField(max_length=255, choices=CRAWL_JOBS_CHOICES, default=CRAWL_SEMESTER, db_index=True)
    semester = models.ForeignKey('Semester', verbose_name=_('semester'), related_name='crawl_jobs', null=True)
    start_date = models.DateTimeField(verbose_name=_('crawl_start_date'), db_index=True, blank=True, null=True)
    end_date = models.DateTimeField(verbose_name=_('crawl_end_date'), blank=True, null=True, db_index=True)
    is_finished = models.BooleanField(verbose_name=_('is_finished'), default=False, db_index=True)

    def __unicode__(self):
        return u'{} {}'.format(self.command, self.semester.title)

    class Meta(object):
        verbose_name = _('crawl_job')
        verbose_name_plural = _('crawl_jobs')
        ordering = ('start_date',)

    def save(self, *args, **kwargs):
        """
        Overrides the default save method to start the command in a new thread
        :param kwargs:
        :param args:
        """
        self.start_date = now()

        def _start_command():
            call_command(self.command, self.semester.title)
            self.is_finished = True
            self.end_date = now()
            super(CrawlJob, self).save(*args, **kwargs)

        if not self.pk:
            thread.start_new_thread(_start_command, ())

        super(CrawlJob, self).save(*args, **kwargs)


class ExportJob(models.Model):
    """
    All exports are organized into export jobs
    """
    command = models.CharField(max_length=255, default=EXPORT_TO_METEOR, db_index=True)
    start_date = models.DateTimeField(verbose_name=_('crawl_start_date'), db_index=True, blank=True, null=True)
    end_date = models.DateTimeField(verbose_name=_('crawl_end_date'), blank=True, null=True, db_index=True)
    is_finished = models.BooleanField(verbose_name=_('is_finished'), default=False, db_index=True)

    def __unicode__(self):
        return self.command

    class Meta(object):
        verbose_name = _('export_job')
        verbose_name_plural = _('export_jobs')
        ordering = ('start_date',)

    def save(self, *args, **kwargs):
        """
        Overrides the default save method to start the command in a new thread
        :param kwargs:
        :param args:
        """
        self.start_date = now()

        def _start_command():
            call_command(self.command)
            self.is_finished = True
            self.end_date = now()
            super(ExportJob, self).save(*args, **kwargs)

        if not self.pk:
            thread.start_new_thread(_start_command, ())

        super(ExportJob, self).save(*args, **kwargs)


class TagCategoriesJob(models.Model):
    command = models.CharField(max_length=255, default=TAG_CATEGROIES, db_index=True)
    start_date = models.DateTimeField(verbose_name=_('tag_categories_start_date'), db_index=True, blank=True, null=True)
    end_date = models.DateTimeField(verbose_name=_('tag_categories_end_date'), blank=True, null=True, db_index=True)
    is_finished = models.BooleanField(verbose_name=_('is_finished'), default=False, db_index=True)

    def __unicode__(self):
        return self.command

    class Meta(object):
        verbose_name = _('tag_categories')
        verbose_name_plural = _('tag_categories')
        ordering = ('start_date',)

    def save(self, *args, **kwargs):
        """
        Overrides the default save method to start the command in a new thread
        :param kwargs:
        :param args:
        """
        self.start_date = now()

        def _start_command():
            call_command(self.command)
            self.is_finished = True
            self.end_date = now()
            super(TagCategoriesJob, self).save(*args, **kwargs)

        if not self.pk:
            thread.start_new_thread(_start_command, ())

        super(TagCategoriesJob, self).save(*args, **kwargs)


class Semester(models.Model):
    """
    All crawls are grouped into semesters
    """
    title = models.CharField(verbose_name=_('title'), max_length=255, db_index=True)
    semester_start = models.DateField(verbose_name=_('semester_start'), blank=True, null=True)
    semester_end = models.DateField(verbose_name=_('semester_end'), blank=True, null=True)
    crawl_date = models.DateTimeField(verbose_name=_('crawl_date'), default=now(), db_index=True)

    class Meta(object):
        verbose_name = _('semester')
        verbose_name_plural = _('semesters')
        ordering = ('semester_start',)

    def __unicode__(self):
        return self.title

    @staticmethod
    def create_or_update(semester_name, reversion_comment=_('create_or_update')):
        """
        Creates a semester object if no one is existing otherwise replaces all attributes with the new ones
        :param semester_name: semester
        :param reversion_comment: optional comment for the reversion plugin
        :return: created or updated semester object
        """
        # reversion support
        with transaction.atomic(), reversion.create_revision():
            # get or create
            try:
                semester = Semester.objects.get(title=semester_name)
            except Semester.DoesNotExist:
                semester = Semester(title=semester_name)
            # update attributes
            semester.crawl_date = now()
            # save with reversion comment
            semester.save()
            reversion.set_comment(reversion_comment)
            return semester


reversion.register(Semester)


class UZHBaseModel(models.Model):
    """
    Abstract base model for uzh model

    uzh_id + semester are a unique identifier
    uzh_url is also a unique identifier, but sometime multiple urls can lead to same item
    """
    uzh_id = models.CharField(verbose_name=_('uzh_id'), max_length=255, db_index=True)
    uzh_url = models.URLField(verbose_name=_('uzh_url'), db_index=True, unique=True)
    semester = models.ForeignKey('Semester', verbose_name=_('semester'), db_index=True)

    class Meta(object):
        abstract = True


MIXED_MODULES = 'mixed_modules'
MANDATORY_MODULES = 'mandatory_modules'
COMPULSORY_ELECTIVE_MODULES = 'compulsory_elective_modules'
ELECTIVE_MODULES = 'elective_modules'

CATEGORY_MODULE_TYPE_CHOICES = (
    (MIXED_MODULES, _('mixed_modules')),
    (MANDATORY_MODULES, _('mandatory_modules')),
    (COMPULSORY_ELECTIVE_MODULES, _('compulsory_elective_modules')),
    (ELECTIVE_MODULES, _('elective_modules')),
)


class Category(UZHBaseModel):
    """
    Taxonomy for all categories.
    """
    title = models.CharField(verbose_name=_('title'), max_length=255, db_index=True)
    parents = models.ManyToManyField('Category', null=True, blank=True, related_name='children')
    level = models.IntegerField(verbose_name=_('level'), db_index=True)
    custom_categories = models.ManyToManyField('CustomCategory', verbose_name=_('custom_categories'),
                                               related_name='categories', null=True, blank=True, db_index=True)
    study_degree = models.TextField(verbose_name=_('degree'), max_length=255, blank=True)
    study_content = models.TextField(verbose_name=_('study_content'), blank=True)
    total_ects = models.IntegerField(verbose_name=_('total_etcs'), blank=True, null=True)
    major_minor_regulations = models.TextField(verbose_name=_('major_minor_regulations'), blank=True)
    precognition = models.TextField(verbose_name=_('precognition'), blank=True)
    field_of_study = models.TextField(verbose_name=_('field_of_study'), blank=True)

    class Meta(object):
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ('uzh_url',)
        # uzh_id + semester are a unique identifier
        unique_together = ('uzh_id', 'semester',)

    def __unicode__(self):
        return self.title

    def natural_key(self):
        return self.uzh_id

    @staticmethod
    def create_or_update(uzh_id, uzh_url, title, level, semester, study_degree='', study_content='', total_ects='0',
                         major_minor_regulations='', precognition='', field_of_study='',
                         reversion_comment=_('create_or_update')):
        """
        Creates a category object if no one is existing otherwise replaces all attributes with the new ones
        :param uzh_id: uzh id for the category object
        :param uzh_url: uzh ur for the category object
        :param title: title of the category object
        :param level: level of the category
        :param semester: semester (together with uzh_id a unique identifier)
        :param study_degree
        :param study_content
        :param total_ects
        :param major_minor_regulations
        :param precognition
        :param field_of_study
        :param reversion_comment: optional comment for the reversion plugin
        :return: created or updated category object
        """
        # reversion support
        with transaction.atomic(), reversion.create_revision():
            # get or create
            try:
                category = Category.objects.get(uzh_id=uzh_id, semester=semester)
            except Category.DoesNotExist:
                category = Category(uzh_id=uzh_id, semester=semester)
            # update attributes
            category.title = title
            category.uzh_url = uzh_url
            category.level = level
            category.study_degree = study_degree
            category.study_content = study_content
            ects = [int(s) for s in total_ects.split() if s.isdigit()]
            category.total_ects = ects[0] if len(ects) > 0 else None
            category.major_minor_regulations = major_minor_regulations
            category.precognition = precognition
            category.field_of_study = field_of_study
            # save with reversion comment
            category.save()
            reversion.set_comment(reversion_comment)
            return category

    def update_parent(self, parent_uzh_id, semester, reversion_comment=_('update_parent')):
        """
        Sets the parent attribute of category object given the uzh id of the parent (and not the django parent object)
        :param parent_uzh_id: uzh id of the parent category
        :param semester: semester
        :param reversion_comment: optional comment for the reversion plugin
        """
        with transaction.atomic(), reversion.create_revision():
            # get parent django object
            parent_category = Category.objects.get(uzh_id=parent_uzh_id, semester=semester)
            # add parent object
            self.parents.add(parent_category)
            # save with reversion comment
            self.save()
            reversion.set_comment(reversion_comment)


class CustomCategory(models.Model):
    """
    Custom categories with self defined hierarchy
    """
    title = models.CharField(verbose_name=_('title'), max_length=255, db_index=True)
    slug = models.SlugField(verbose_name=_('slug'), db_index=True)
    module_type = models.CharField(max_length=255, verbose_name=_('module_type'), choices=CATEGORY_MODULE_TYPE_CHOICES,
                                   default=MIXED_MODULES, db_index=True)
    parents = models.ManyToManyField('CustomCategory', null=True, blank=True, related_name='children')

    class Meta(object):
        verbose_name = _('custom_category')
        verbose_name_plural = _('custom_categories')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


reversion.register(CustomCategory)


class Module(UZHBaseModel):
    """
    Model representation of a module
    """
    title = models.CharField(verbose_name=_('title'), max_length=255, db_index=True)
    uzh_code = models.CharField(verbose_name=_('uzh_code'), max_length=255, db_index=True)
    ects = models.DecimalField(verbose_name=_('etcs'), max_digits=4, decimal_places=1, db_index=True)
    frequency = models.ForeignKey('ModuleFrequency', verbose_name=_('module_frequency'), related_name='modules',
                                  null=True, blank=True)
    description = models.TextField(verbose_name=_('description'), blank=True)
    processed_description = models.TextField(verbose_name=_('processed_description'), blank=True)
    tf_idf_words = models.TextField(verbose_name=_('processed_description'), blank=True)
    precognition = models.TextField(verbose_name=_('precognition'), blank=True)
    requirements = models.TextField(verbose_name=_('requirements'), blank=True)
    teaching_materials = models.TextField(verbose_name=_('teaching_materials'), blank=True)
    learning_target = models.TextField(verbose_name=_('learning_target'), blank=True)
    target_audience = models.TextField(verbose_name=_('target_audience'), blank=True)
    achievement_test = models.TextField(verbose_name=_('achievement_test'), blank=True)
    grading_scale = models.ForeignKey('ModuleGradingScale', verbose_name=_('module_grading_scale'),
                                      related_name='modules', null=True, blank=True)
    repeatability = models.ForeignKey('ModuleRepeatability', verbose_name=_('module_repeatability'),
                                      related_name='modules', null=True, blank=True)
    language = models.ForeignKey('ModuleLanguage', verbose_name=_('module_language'),
                                 related_name='modules', null=True, blank=True)
    further_information = models.TextField(verbose_name=_('further_information'), blank=True)
    booking_deadline = models.ForeignKey('ModuleDeadline', verbose_name=_('booking_deadline'),
                                         related_name='booking_modules', null=True, blank=True)
    cancellation_deadline = models.ForeignKey('ModuleDeadline', verbose_name=_('cancellation_deadline'),
                                              related_name='cancellation_modules', null=True, blank=True)
    organizational_units = models.ManyToManyField('ModuleOrganizational', verbose_name=_('module_organizational'),
                                                  related_name='modules', null=True, blank=True)
    persons_in_charge = models.ManyToManyField('Person', verbose_name=_('person_in_charge'), related_name='modules',
                                               null=True, blank=True)
    categories = models.ManyToManyField('Category', verbose_name=_('categories'), db_index=True, related_name='modules')
    features = models.ManyToManyField('ModuleFeature', through='ModuleFeatureRelation',
                                      verbose_name=_('module_features'), related_name='modules')

    class Meta(object):
        verbose_name = _('module')
        verbose_name_plural = _('modules')
        ordering = ('uzh_url',)
        # uzh_id + semester are a unique identifier
        unique_together = ('uzh_id', 'semester',)

    def __unicode__(self):
        return self.title

    @staticmethod
    def create_or_update(uzh_id, uzh_url, semester, title, uzh_code, category, ects=0, frequency='', description='',
                         precognition='', requirements='', teaching_materials='', learning_target='',
                         target_audience='', person_ids='',
                         achievement_test='', grading_scale='', repeatability='', language='', further_information='',
                         booking_deadline='', cancellation_deadline='', organizational_units='',
                         reversion_comment=_('create_or_update')):
        """
        :param organizational_units
        :param uzh_id:
        :param uzh_url:
        :param semester:
        :param title:
        :param uzh_code:
        :param category:
        :param ects:
        :param frequency:
        :param description:
        :param precognition:
        :param requirements:
        :param teaching_materials:
        :param learning_target:
        :param target_audience:
        :param person_ids
        :param achievement_test:
        :param grading_scale:
        :param repeatability:
        :param language:
        :param further_information:
        :param booking_deadline:
        :param cancellation_deadline:
        :param organizational_units
        :param reversion_comment:
        :return:
        """
        # reversion support
        with transaction.atomic(), reversion.create_revision():
            # get or create
            try:
                module = Module.objects.get(uzh_id=uzh_id, semester=semester)
            except Module.DoesNotExist:
                module = Module(uzh_id=uzh_id, semester=semester)
            # update attributes
            module.title = title
            module.uzh_url = uzh_url
            module.uzh_code = uzh_code
            module.ects = ects
            module.frequency = SimpleAttribute.get_simple_model_object(ModuleFrequency, frequency)
            module.description = description
            module.precognition = precognition
            module.requirements = requirements
            module.teaching_materials = teaching_materials
            module.learning_target = learning_target
            module.target_audience = target_audience
            module.save()
            for person_id in person_ids:
                try:
                    person = Person.objects.get(uzh_id=person_id, semester=semester)
                except Person.DoesNotExist:
                    person = Person(uzh_id=person_id, semester=semester)
                module.persons_in_charge.add(person)
            module.achievement_test = achievement_test
            module.grading_scale = SimpleAttribute.get_simple_model_object(ModuleGradingScale, grading_scale)
            module.repeatability = SimpleAttribute.get_simple_model_object(ModuleRepeatability, repeatability)
            module.language = SimpleAttribute.get_simple_model_object(ModuleLanguage, language)
            module.further_information = further_information
            module.booking_deadline = SimpleAttribute.get_simple_model_object(ModuleDeadline, booking_deadline)
            module.cancellation_deadline = SimpleAttribute.get_simple_model_object(ModuleDeadline,
                                                                                   cancellation_deadline)
            for organizational_unit in organizational_units:
                organizational_object, _ = ModuleOrganizational.objects.get_or_create(
                        title=organizational_unit['title'],
                        defaults={'url': organizational_unit['url']})
                module.organizational_units.add(organizational_object)
            # save with reversion comment
            module.save()
            if Module.new_category_is_deepest_available(category, module.categories.all()):
                module.categories.add(category)
            module.save()
            reversion.set_comment(reversion_comment)
            return module

    @staticmethod
    def new_category_is_deepest_available(category, already_assigned_categories):
        category_set = []
        Module.get_all_children(category, category_set)
        intersection = [val for val in category_set if val in already_assigned_categories]
        if len(intersection) == 0:
            return True
        else:
            return False

    @staticmethod
    def get_all_children(category, category_set):
        category_set.append(category)
        for child in category.children.all():
            Module.get_all_children(child, category_set)


reversion.register(Module)


class SimpleAttribute(models.Model):
    """
    Abstract base class for simple module attributes modeled as forein keys
    """
    title = models.CharField(verbose_name=_('title'), max_length=255, db_index=True, unique=True)

    class Meta(object):
        abstract = True

    def natural_key(self):
        return self.title

    @staticmethod
    def get_simple_model_object(model_class, value):
        """
        Get or creates a object of given class and value
        :param value: value of object
        :param model_class: class of object
        """
        if value:
            model_object, _ = model_class.objects.get_or_create(title=value)
            return model_object
        else:
            return None


class ModuleFrequency(SimpleAttribute):
    """
    Different values how often a module is offered (Dauer und Angebotsmuster)
    """

    class Meta(object):
        verbose_name = _('module_frequency')
        verbose_name_plural = _('module_frequencies')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


reversion.register(ModuleFrequency)


class ModuleGradingScale(SimpleAttribute):
    """
    Different values for grading scale
    """

    class Meta(object):
        verbose_name = _('module_grading_scale')
        verbose_name_plural = _('module_grading_scales')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


reversion.register(ModuleGradingScale)


class ModuleRepeatability(SimpleAttribute):
    """
    Different values for repeatability
    """

    class Meta(object):
        verbose_name = _('module_repeatability')
        verbose_name_plural = _('module_repeatabilities')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


reversion.register(ModuleRepeatability)


class ModuleLanguage(SimpleAttribute):
    """
    Different values for languages
    """

    class Meta(object):
        verbose_name = _('module_language')
        verbose_name_plural = _('module_languages')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


reversion.register(ModuleLanguage)


class ModuleDeadline(SimpleAttribute):
    """
    Different values for time deadlines (booking, cancellation)
    """

    class Meta(object):
        verbose_name = _('module_deadline')
        verbose_name_plural = _('module_deadlines')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


class ModuleOrganizational(SimpleAttribute):
    """
    Organisations listed in modules
    """
    url = models.URLField(verbose_name=_('url'), db_index=True, blank=True, null=True)

    class Meta(object):
        verbose_name = _('module_organizational')
        verbose_name_plural = _('module_organizationals')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


reversion.register(ModuleDeadline)


class ModuleFeature(models.Model):
    """
    Different values for time deadlines (booking, cancellation)
    Could also be modelled in start and end time
    """

    title = models.TextField(verbose_name='title', blank=True)

    class Meta(object):
        verbose_name = _('module_feature')
        verbose_name_plural = _('module_features')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


reversion.register(ModuleFeature)


class ModuleFeatureRelation(models.Model):
    """
    The feature - module relation has a weight as additional attribute
    """

    module = models.ForeignKey('Module', verbose_name=_('module'))
    feature = models.ForeignKey('ModuleFeature', verbose_name=_('module_feature'))
    weight = models.DecimalField(_('weight'), max_digits=6, decimal_places=5, db_index=True)

    class Meta(object):
        verbose_name = _('module_feature_relation')
        verbose_name_plural = _('module_feature_relations')
        ordering = ('module', 'feature')

    def __unicode__(self):
        return u'{} {}'.format(self.module, self.feature)


reversion.register(ModuleFeatureRelation)


class Lecture(UZHBaseModel):
    """
    Model representation of a lecture
    """

    title = models.CharField(verbose_name=_('title'), max_length=255, db_index=True)
    module = models.ForeignKey('Module', verbose_name=_('module'), db_index=True, related_name='lectures')
    uzh_lecture_id = models.CharField(verbose_name=_('uzh_lecture_id'), max_length=255, db_index=True)
    uzh_lecture_code = models.CharField(verbose_name=_('uzh_lecture_code'), max_length=255, db_index=True)
    category = models.ForeignKey('LectureCategory', verbose_name=_('module_category'),
                                 related_name='lectures', null=True, blank=True)
    content = models.TextField(verbose_name=_('content'), blank=True)
    teaching_materials = models.TextField(verbose_name=_('teaching_materials'), blank=True)

    class Meta(object):
        verbose_name = _('lecture')
        verbose_name_plural = _('lectures')
        # uzh_id + semester are a unique identifier
        unique_together = ('uzh_id', 'semester',)
        ordering = ('uzh_url',)

    def __unicode__(self):
        return self.title

    @staticmethod
    def create_or_update(uzh_id, uzh_url, title, uzh_lecture_id, uzh_lecture_code, category, content,
                         teaching_materials, semester, module, reversion_comment=_('create_or_update')):
        """
        Creates a lecture object if no one is existing otherwise replaces all attributes with the new ones
        :param content:
        :param category:
        :param uzh_lecture_code:
        :param uzh_lecture_id:
        :param teaching_materials:
        :param uzh_id:
        :param uzh_url:
        :param title:
        :param semester:
        :param module:
        :param reversion_comment:
        :return:
        """
        # reversion support
        with transaction.atomic(), reversion.create_revision():
            # get or create
            try:
                lecture = Lecture.objects.get(uzh_id=uzh_id, semester=semester)
            except Lecture.DoesNotExist:
                lecture = Lecture(uzh_id=uzh_id, semester=semester)
            # update attributes
            lecture.title = title
            lecture.uzh_url = uzh_url
            lecture.uzh_lecture_id = uzh_lecture_id
            lecture.uzh_lecture_code = uzh_lecture_code
            lecture.category = SimpleAttribute.get_simple_model_object(LectureCategory, category)
            lecture.content = content
            lecture.teaching_materials = teaching_materials
            # save with reversion comment
            lecture.module = module
            lecture.save()
            reversion.set_comment(reversion_comment)
            return lecture


reversion.register(Lecture)


class LectureCategory(SimpleAttribute):
    """
    Categories of lectures
    """
    is_active = models.BooleanField(verbose_name=_('is_active'), default=True)

    class Meta(object):
        verbose_name = _('lecture_category')
        verbose_name_plural = _('lecture_categories')
        ordering = ('title',)

    def __unicode__(self):
        return self.title


class LectureScheduleItem(models.Model):
    """
    Model representation of a lecture
    """

    lecture = models.ForeignKey('Lecture', verbose_name=_('lecture'), related_name='lecture_schedule_item')
    start_datetime = models.DateTimeField(verbose_name=_('start_datetime'), db_index=True)
    end_datetime = models.DateTimeField(verbose_name=_('end_datetime'), db_index=True)
    rooms = models.ManyToManyField('Room', verbose_name=_('room'), related_name='lecture_schedule_item', null=True,
                                   blank=True)
    lecturers = models.ManyToManyField('Person', verbose_name=_('person'), related_name='lecture_schedule_item',
                                       null=True, blank=True)

    class Meta(object):
        verbose_name = _('lecture_schedule_item')
        verbose_name_plural = _('lecture_schedule_items')
        ordering = ('start_datetime',)

    def __unicode__(self):
        return u'{}: {} - {}'.format(self.lecture, self.start_datetime, self.end_datetime)

    @staticmethod
    def create_or_update(lecture, start_datetime, end_datetime, room_ids, lecturer_ids,
                         reversion_comment=_('create_or_update')):
        """
        Creates a persons object if no one is existing otherwise replaces all attributes with the new ones
        :param lecturer_ids:
        :param room_ids:
        :param lecture
        :param start_datetime:
        :param end_datetime:
        :param reversion_comment:
        :return:
        """
        # reversion support
        with transaction.atomic(), reversion.create_revision():
            # get or create
            try:
                lecture_schedule_item = LectureScheduleItem.objects.get(lecture=lecture, start_datetime=start_datetime,
                                                                        end_datetime=end_datetime)
            except LectureScheduleItem.DoesNotExist:
                lecture_schedule_item = LectureScheduleItem(lecture=lecture, start_datetime=start_datetime,
                                                            end_datetime=end_datetime)
            # update attributes
            lecture_schedule_item.save()

            for room_id in room_ids:
                try:
                    room = Room.objects.get(uzh_id=room_id, semester=lecture.semester)
                except Room.DoesNotExist:
                    room = Room(uzh_id=room_id, semester=lecture.semester)
                lecture_schedule_item.rooms.add(room)
            for lecturer_id in lecturer_ids:
                try:
                    lecturer = Person.objects.get(uzh_id=lecturer_id, semester=lecture.semester)
                except Person.DoesNotExist:
                    lecturer = Person(uzh_id=lecturer_id, semester=lecture.semester)
                lecture_schedule_item.lecturers.add(lecturer)
            lecture_schedule_item.save()
            reversion.set_comment(reversion_comment)
            return lecture_schedule_item


reversion.register(LectureScheduleItem)


class Person(UZHBaseModel):
    """
    Model representation of a person
    """

    firstname = models.CharField(verbose_name=_('firstname'), max_length=255, db_index=True)
    lastname = models.CharField(verbose_name=_('lastname'), max_length=255, db_index=True)
    title = models.CharField(verbose_name=_('title'), max_length=255, db_index=True)
    position = models.CharField(verbose_name=_('position'), max_length=255, db_index=True)

    class Meta(object):
        verbose_name = _('person')
        verbose_name_plural = _('persons')
        ordering = ('firstname', 'lastname')
        # uzh_id + semester are a unique identifier
        unique_together = ('uzh_id', 'semester',)

    def __unicode__(self):
        return self.firstname + ' ' + self.lastname

    @staticmethod
    def create_or_update(semester, uzh_id, uzh_url, firstname, lastname, title, position,
                         reversion_comment=_('create_or_update')):
        """
        Creates a persons object if no one is existing otherwise replaces all attributes with the new ones
        :param semester:
        :param uzh_id:
        :param uzh_url:
        :param firstname:
        :param lastname:
        :param title:
        :param position:
        :param reversion_comment:
        :return:
        """
        # reversion support
        with transaction.atomic(), reversion.create_revision():
            # get or create
            try:
                person = Person.objects.get(uzh_id=uzh_id, semester=semester)
            except Person.DoesNotExist:
                person = Person(uzh_id=uzh_id, semester=semester)
            # update attributes
            person.uzh_url = uzh_url
            person.firstname = firstname
            person.lastname = lastname
            person.title = title
            person.position = position
            # save with reversion comment
            person.save()
            reversion.set_comment(reversion_comment)
            return person


reversion.register(Person)


class Room(UZHBaseModel):
    """
    Model representation of a Room
    """
    name = models.CharField(verbose_name=_('name'), max_length=255, db_index=True)
    street = models.CharField(verbose_name=_('street'), max_length=255, db_index=True)
    address = models.CharField(verbose_name=_('address'), max_length=255, db_index=True)

    class Meta(object):
        verbose_name = _('room')
        verbose_name_plural = _('rooms')
        # uzh_id + semester are a unique identifier
        unique_together = ('uzh_id', 'semester',)

    def __unicode__(self):
        return self.name

    @staticmethod
    def create_or_update(semester, uzh_id, uzh_url, name, street, address,
                         reversion_comment=_('create_or_update')):
        """
        Creates a room object if no one is existing otherwise replaces all attributes with the new ones
        :param semester:
        :param uzh_id:
        :param uzh_url:
        :param name:
        :param street:
        :param address:
        :param reversion_comment:
        :return:
        """
        # reversion support
        with transaction.atomic(), reversion.create_revision():
            # get or create
            try:
                room = Room.objects.get(uzh_id=uzh_id, semester=semester)
            except Room.DoesNotExist:
                room = Room(uzh_id=uzh_id, semester=semester)
            # update attributes
            room.uzh_url = uzh_url
            room.name = name
            room.street = street
            room.address = address
            # save with reversion comment
            room.save()
            reversion.set_comment(reversion_comment)
            return room


reversion.register(Room)
