# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'ModuleFeature.title'
        db.delete_column(u'crawler_modulefeature', 'title')


    def backwards(self, orm):
        # Adding field 'ModuleFeature.title'
        db.add_column(u'crawler_modulefeature', 'title',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, unique=True, db_index=True),
                      keep_default=False)


    models = {
        u'crawler.category': {
            'Meta': {'ordering': "('uzh_url',)", 'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Category'},
            'custom_categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'categories'", 'to': u"orm['crawler.CustomCategory']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'db_index': 'True'}),
            'field_of_study': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'major_minor_regulations': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'parents': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.Category']"}),
            'precognition': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'study_content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'study_degree': ('django.db.models.fields.TextField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'total_ects': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.crawljob': {
            'Meta': {'ordering': "('start_date',)", 'object_name': 'CrawlJob'},
            'command': ('django.db.models.fields.CharField', [], {'default': "'crawl_semester'", 'max_length': '255', 'db_index': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_finished': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'crawl_jobs'", 'null': 'True', 'to': u"orm['crawler.Semester']"}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'crawler.customcategory': {
            'Meta': {'ordering': "('title',)", 'object_name': 'CustomCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module_type': ('django.db.models.fields.CharField', [], {'default': "'mixed_modules'", 'max_length': '255', 'db_index': 'True'}),
            'parents': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.CustomCategory']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.exportjob': {
            'Meta': {'ordering': "('start_date',)", 'object_name': 'ExportJob'},
            'command': ('django.db.models.fields.CharField', [], {'default': "'export_to_meteor'", 'max_length': '255', 'db_index': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_finished': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'crawler.lecture': {
            'Meta': {'ordering': "('uzh_url',)", 'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Lecture'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'lectures'", 'null': 'True', 'to': u"orm['crawler.LectureCategory']"}),
            'content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'lectures'", 'to': u"orm['crawler.Module']"}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'teaching_materials': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_lecture_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_lecture_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.lecturecategory': {
            'Meta': {'ordering': "('title',)", 'object_name': 'LectureCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.lecturescheduleitem': {
            'Meta': {'ordering': "('start_datetime',)", 'object_name': 'LectureScheduleItem'},
            'end_datetime': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lecture': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'lecture_schedule_item'", 'to': u"orm['crawler.Lecture']"}),
            'lecturers': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'lecture_schedule_item'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.Person']"}),
            'rooms': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'lecture_schedule_item'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.Room']"}),
            'start_datetime': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'})
        },
        u'crawler.module': {
            'Meta': {'ordering': "('uzh_url',)", 'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Module'},
            'achievement_test': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'booking_deadline': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'booking_modules'", 'null': 'True', 'to': u"orm['crawler.ModuleDeadline']"}),
            'cancellation_deadline': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cancellation_modules'", 'null': 'True', 'to': u"orm['crawler.ModuleDeadline']"}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'db_index': 'True', 'related_name': "'modules'", 'symmetrical': 'False', 'to': u"orm['crawler.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'ects': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '1', 'db_index': 'True'}),
            'features': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'modules'", 'symmetrical': 'False', 'through': u"orm['crawler.ModuleFeatureRelation']", 'to': u"orm['crawler.ModuleFeature']"}),
            'frequency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'to': u"orm['crawler.ModuleFrequency']"}),
            'further_information': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'grading_scale': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'to': u"orm['crawler.ModuleGradingScale']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'to': u"orm['crawler.ModuleLanguage']"}),
            'learning_target': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'organizational_units': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.ModuleOrganizational']"}),
            'persons_in_charge': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.Person']"}),
            'precognition': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'processed_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'repeatability': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'to': u"orm['crawler.ModuleRepeatability']"}),
            'requirements': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'target_audience': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'teaching_materials': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.moduledeadline': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleDeadline'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.modulefeature': {
            'Meta': {'object_name': 'ModuleFeature'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'crawler.modulefeaturerelation': {
            'Meta': {'ordering': "('module', 'feature')", 'object_name': 'ModuleFeatureRelation'},
            'feature': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.ModuleFeature']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Module']"}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '5', 'db_index': 'True'})
        },
        u'crawler.modulefrequency': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleFrequency'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.modulegradingscale': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleGradingScale'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.modulelanguage': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleLanguage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.moduleorganizational': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleOrganizational'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'db_index': 'True', 'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'crawler.modulerepeatability': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleRepeatability'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.person': {
            'Meta': {'ordering': "('firstname', 'lastname')", 'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Person'},
            'firstname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.room': {
            'Meta': {'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Room'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.semester': {
            'Meta': {'ordering': "('semester_start',)", 'object_name': 'Semester'},
            'crawl_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2016, 1, 22, 0, 0)', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'semester_end': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'semester_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.tagcategoriesjob': {
            'Meta': {'ordering': "('start_date',)", 'object_name': 'TagCategoriesJob'},
            'command': ('django.db.models.fields.CharField', [], {'default': "'tag_categories'", 'max_length': '255', 'db_index': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_finished': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['crawler']