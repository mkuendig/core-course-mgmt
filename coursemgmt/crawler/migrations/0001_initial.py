# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CrawlJob'
        db.create_table(u'crawler_crawljob', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('command', self.gf('django.db.models.fields.CharField')(default='crawl_semester', max_length=255, db_index=True)),
            ('semester', self.gf('django.db.models.fields.related.ForeignKey')(related_name='crawl_jobs', null=True, to=orm['crawler.Semester'])),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('is_finished', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['CrawlJob'])

        # Adding model 'ExportJob'
        db.create_table(u'crawler_exportjob', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('command', self.gf('django.db.models.fields.CharField')(default='export_to_meteor', max_length=255, db_index=True)),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')(db_index=True, null=True, blank=True)),
            ('is_finished', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['ExportJob'])

        # Adding model 'Semester'
        db.create_table(u'crawler_semester', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('semester_start', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('semester_end', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('crawl_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 8, 12, 0, 0), db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['Semester'])

        # Adding model 'Category'
        db.create_table(u'crawler_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('uzh_id', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('uzh_url', self.gf('django.db.models.fields.URLField')(unique=True, max_length=200, db_index=True)),
            ('semester', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crawler.Semester'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('level', self.gf('django.db.models.fields.IntegerField')(db_index=True)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True, db_index=True)),
            ('study_degree', self.gf('django.db.models.fields.TextField')(max_length=255, blank=True)),
            ('study_content', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('total_ects', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('major_minor_regulations', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('precognition', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('field_of_study', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'crawler', ['Category'])

        # Adding unique constraint on 'Category', fields ['uzh_id', 'semester']
        db.create_unique(u'crawler_category', ['uzh_id', 'semester_id'])

        # Adding M2M table for field parents on 'Category'
        m2m_table_name = db.shorten_name(u'crawler_category_parents')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_category', models.ForeignKey(orm[u'crawler.category'], null=False)),
            ('to_category', models.ForeignKey(orm[u'crawler.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_category_id', 'to_category_id'])

        # Adding M2M table for field custom_categories on 'Category'
        m2m_table_name = db.shorten_name(u'crawler_category_custom_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('category', models.ForeignKey(orm[u'crawler.category'], null=False)),
            ('customcategory', models.ForeignKey(orm[u'crawler.customcategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['category_id', 'customcategory_id'])

        # Adding model 'CustomCategory'
        db.create_table(u'crawler_customcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('module_type', self.gf('django.db.models.fields.CharField')(default='mixed_modules', max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['CustomCategory'])

        # Adding M2M table for field parents on 'CustomCategory'
        m2m_table_name = db.shorten_name(u'crawler_customcategory_parents')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_customcategory', models.ForeignKey(orm[u'crawler.customcategory'], null=False)),
            ('to_customcategory', models.ForeignKey(orm[u'crawler.customcategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_customcategory_id', 'to_customcategory_id'])

        # Adding model 'Module'
        db.create_table(u'crawler_module', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('uzh_id', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('uzh_url', self.gf('django.db.models.fields.URLField')(unique=True, max_length=200, db_index=True)),
            ('semester', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crawler.Semester'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('uzh_code', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('ects', self.gf('django.db.models.fields.DecimalField')(max_digits=4, decimal_places=1, db_index=True)),
            ('frequency', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='modules', null=True, to=orm['crawler.ModuleFrequency'])),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('precognition', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('requirements', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('teaching_materials', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('learning_target', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('target_audience', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('achievement_test', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('grading_scale', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='modules', null=True, to=orm['crawler.ModuleGradingScale'])),
            ('repeatability', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='modules', null=True, to=orm['crawler.ModuleRepeatability'])),
            ('language', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='modules', null=True, to=orm['crawler.ModuleLanguage'])),
            ('further_information', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('booking_deadline', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='booking_modules', null=True, to=orm['crawler.ModuleDeadline'])),
            ('cancellation_deadline', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cancellation_modules', null=True, to=orm['crawler.ModuleDeadline'])),
        ))
        db.send_create_signal(u'crawler', ['Module'])

        # Adding unique constraint on 'Module', fields ['uzh_id', 'semester']
        db.create_unique(u'crawler_module', ['uzh_id', 'semester_id'])

        # Adding M2M table for field organizational_units on 'Module'
        m2m_table_name = db.shorten_name(u'crawler_module_organizational_units')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('module', models.ForeignKey(orm[u'crawler.module'], null=False)),
            ('moduleorganizational', models.ForeignKey(orm[u'crawler.moduleorganizational'], null=False))
        ))
        db.create_unique(m2m_table_name, ['module_id', 'moduleorganizational_id'])

        # Adding M2M table for field persons_in_charge on 'Module'
        m2m_table_name = db.shorten_name(u'crawler_module_persons_in_charge')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('module', models.ForeignKey(orm[u'crawler.module'], null=False)),
            ('person', models.ForeignKey(orm[u'crawler.person'], null=False))
        ))
        db.create_unique(m2m_table_name, ['module_id', 'person_id'])

        # Adding M2M table for field categories on 'Module'
        m2m_table_name = db.shorten_name(u'crawler_module_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('module', models.ForeignKey(orm[u'crawler.module'], null=False)),
            ('category', models.ForeignKey(orm[u'crawler.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['module_id', 'category_id'])

        # Adding model 'ModuleFrequency'
        db.create_table(u'crawler_modulefrequency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['ModuleFrequency'])

        # Adding model 'ModuleGradingScale'
        db.create_table(u'crawler_modulegradingscale', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['ModuleGradingScale'])

        # Adding model 'ModuleRepeatability'
        db.create_table(u'crawler_modulerepeatability', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['ModuleRepeatability'])

        # Adding model 'ModuleLanguage'
        db.create_table(u'crawler_modulelanguage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['ModuleLanguage'])

        # Adding model 'ModuleDeadline'
        db.create_table(u'crawler_moduledeadline', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['ModuleDeadline'])

        # Adding model 'ModuleOrganizational'
        db.create_table(u'crawler_moduleorganizational', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
            ('url', self.gf('django.db.models.fields.URLField')(db_index=True, max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'crawler', ['ModuleOrganizational'])

        # Adding model 'ModuleKeyword'
        db.create_table(u'crawler_modulekeyword', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['ModuleKeyword'])

        # Adding model 'ModuleKeywordRelation'
        db.create_table(u'crawler_modulekeywordrelation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('module', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crawler.Module'])),
            ('keyword', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crawler.ModuleKeyword'])),
            ('weight', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=5, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['ModuleKeywordRelation'])

        # Adding model 'Lecture'
        db.create_table(u'crawler_lecture', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('uzh_id', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('uzh_url', self.gf('django.db.models.fields.URLField')(unique=True, max_length=200, db_index=True)),
            ('semester', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crawler.Semester'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('module', self.gf('django.db.models.fields.related.ForeignKey')(related_name='lectures', to=orm['crawler.Module'])),
            ('uzh_lecture_id', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('uzh_lecture_code', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='lectures', null=True, to=orm['crawler.LectureCategory'])),
            ('content', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('teaching_materials', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'crawler', ['Lecture'])

        # Adding unique constraint on 'Lecture', fields ['uzh_id', 'semester']
        db.create_unique(u'crawler_lecture', ['uzh_id', 'semester_id'])

        # Adding model 'LectureCategory'
        db.create_table(u'crawler_lecturecategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['LectureCategory'])

        # Adding model 'LectureScheduleItem'
        db.create_table(u'crawler_lecturescheduleitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lecture', self.gf('django.db.models.fields.related.ForeignKey')(related_name='lecture_schedule_item', to=orm['crawler.Lecture'])),
            ('start_datetime', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('end_datetime', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['LectureScheduleItem'])

        # Adding M2M table for field rooms on 'LectureScheduleItem'
        m2m_table_name = db.shorten_name(u'crawler_lecturescheduleitem_rooms')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('lecturescheduleitem', models.ForeignKey(orm[u'crawler.lecturescheduleitem'], null=False)),
            ('room', models.ForeignKey(orm[u'crawler.room'], null=False))
        ))
        db.create_unique(m2m_table_name, ['lecturescheduleitem_id', 'room_id'])

        # Adding M2M table for field lecturers on 'LectureScheduleItem'
        m2m_table_name = db.shorten_name(u'crawler_lecturescheduleitem_lecturers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('lecturescheduleitem', models.ForeignKey(orm[u'crawler.lecturescheduleitem'], null=False)),
            ('person', models.ForeignKey(orm[u'crawler.person'], null=False))
        ))
        db.create_unique(m2m_table_name, ['lecturescheduleitem_id', 'person_id'])

        # Adding model 'Person'
        db.create_table(u'crawler_person', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('uzh_id', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('uzh_url', self.gf('django.db.models.fields.URLField')(unique=True, max_length=200, db_index=True)),
            ('semester', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crawler.Semester'])),
            ('firstname', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('lastname', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('position', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['Person'])

        # Adding unique constraint on 'Person', fields ['uzh_id', 'semester']
        db.create_unique(u'crawler_person', ['uzh_id', 'semester_id'])

        # Adding model 'Room'
        db.create_table(u'crawler_room', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('uzh_id', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('uzh_url', self.gf('django.db.models.fields.URLField')(unique=True, max_length=200, db_index=True)),
            ('semester', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['crawler.Semester'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255, db_index=True)),
        ))
        db.send_create_signal(u'crawler', ['Room'])

        # Adding unique constraint on 'Room', fields ['uzh_id', 'semester']
        db.create_unique(u'crawler_room', ['uzh_id', 'semester_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Room', fields ['uzh_id', 'semester']
        db.delete_unique(u'crawler_room', ['uzh_id', 'semester_id'])

        # Removing unique constraint on 'Person', fields ['uzh_id', 'semester']
        db.delete_unique(u'crawler_person', ['uzh_id', 'semester_id'])

        # Removing unique constraint on 'Lecture', fields ['uzh_id', 'semester']
        db.delete_unique(u'crawler_lecture', ['uzh_id', 'semester_id'])

        # Removing unique constraint on 'Module', fields ['uzh_id', 'semester']
        db.delete_unique(u'crawler_module', ['uzh_id', 'semester_id'])

        # Removing unique constraint on 'Category', fields ['uzh_id', 'semester']
        db.delete_unique(u'crawler_category', ['uzh_id', 'semester_id'])

        # Deleting model 'CrawlJob'
        db.delete_table(u'crawler_crawljob')

        # Deleting model 'ExportJob'
        db.delete_table(u'crawler_exportjob')

        # Deleting model 'Semester'
        db.delete_table(u'crawler_semester')

        # Deleting model 'Category'
        db.delete_table(u'crawler_category')

        # Removing M2M table for field parents on 'Category'
        db.delete_table(db.shorten_name(u'crawler_category_parents'))

        # Removing M2M table for field custom_categories on 'Category'
        db.delete_table(db.shorten_name(u'crawler_category_custom_categories'))

        # Deleting model 'CustomCategory'
        db.delete_table(u'crawler_customcategory')

        # Removing M2M table for field parents on 'CustomCategory'
        db.delete_table(db.shorten_name(u'crawler_customcategory_parents'))

        # Deleting model 'Module'
        db.delete_table(u'crawler_module')

        # Removing M2M table for field organizational_units on 'Module'
        db.delete_table(db.shorten_name(u'crawler_module_organizational_units'))

        # Removing M2M table for field persons_in_charge on 'Module'
        db.delete_table(db.shorten_name(u'crawler_module_persons_in_charge'))

        # Removing M2M table for field categories on 'Module'
        db.delete_table(db.shorten_name(u'crawler_module_categories'))

        # Deleting model 'ModuleFrequency'
        db.delete_table(u'crawler_modulefrequency')

        # Deleting model 'ModuleGradingScale'
        db.delete_table(u'crawler_modulegradingscale')

        # Deleting model 'ModuleRepeatability'
        db.delete_table(u'crawler_modulerepeatability')

        # Deleting model 'ModuleLanguage'
        db.delete_table(u'crawler_modulelanguage')

        # Deleting model 'ModuleDeadline'
        db.delete_table(u'crawler_moduledeadline')

        # Deleting model 'ModuleOrganizational'
        db.delete_table(u'crawler_moduleorganizational')

        # Deleting model 'ModuleKeyword'
        db.delete_table(u'crawler_modulekeyword')

        # Deleting model 'ModuleKeywordRelation'
        db.delete_table(u'crawler_modulekeywordrelation')

        # Deleting model 'Lecture'
        db.delete_table(u'crawler_lecture')

        # Deleting model 'LectureCategory'
        db.delete_table(u'crawler_lecturecategory')

        # Deleting model 'LectureScheduleItem'
        db.delete_table(u'crawler_lecturescheduleitem')

        # Removing M2M table for field rooms on 'LectureScheduleItem'
        db.delete_table(db.shorten_name(u'crawler_lecturescheduleitem_rooms'))

        # Removing M2M table for field lecturers on 'LectureScheduleItem'
        db.delete_table(db.shorten_name(u'crawler_lecturescheduleitem_lecturers'))

        # Deleting model 'Person'
        db.delete_table(u'crawler_person')

        # Deleting model 'Room'
        db.delete_table(u'crawler_room')


    models = {
        u'crawler.category': {
            'Meta': {'ordering': "('uzh_url',)", 'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Category'},
            'custom_categories': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'categories'", 'to': u"orm['crawler.CustomCategory']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True', 'db_index': 'True'}),
            'field_of_study': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'level': ('django.db.models.fields.IntegerField', [], {'db_index': 'True'}),
            'major_minor_regulations': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'parents': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.Category']"}),
            'precognition': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'study_content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'study_degree': ('django.db.models.fields.TextField', [], {'max_length': '255', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'total_ects': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.crawljob': {
            'Meta': {'ordering': "('start_date',)", 'object_name': 'CrawlJob'},
            'command': ('django.db.models.fields.CharField', [], {'default': "'crawl_semester'", 'max_length': '255', 'db_index': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_finished': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'crawl_jobs'", 'null': 'True', 'to': u"orm['crawler.Semester']"}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'crawler.customcategory': {
            'Meta': {'ordering': "('title',)", 'object_name': 'CustomCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module_type': ('django.db.models.fields.CharField', [], {'default': "'mixed_modules'", 'max_length': '255', 'db_index': 'True'}),
            'parents': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.CustomCategory']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.exportjob': {
            'Meta': {'ordering': "('start_date',)", 'object_name': 'ExportJob'},
            'command': ('django.db.models.fields.CharField', [], {'default': "'export_to_meteor'", 'max_length': '255', 'db_index': 'True'}),
            'end_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_finished': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'crawler.lecture': {
            'Meta': {'ordering': "('uzh_url',)", 'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Lecture'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'lectures'", 'null': 'True', 'to': u"orm['crawler.LectureCategory']"}),
            'content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'lectures'", 'to': u"orm['crawler.Module']"}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'teaching_materials': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_lecture_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_lecture_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.lecturecategory': {
            'Meta': {'ordering': "('title',)", 'object_name': 'LectureCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.lecturescheduleitem': {
            'Meta': {'ordering': "('start_datetime',)", 'object_name': 'LectureScheduleItem'},
            'end_datetime': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lecture': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'lecture_schedule_item'", 'to': u"orm['crawler.Lecture']"}),
            'lecturers': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'lecture_schedule_item'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.Person']"}),
            'rooms': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'lecture_schedule_item'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.Room']"}),
            'start_datetime': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'})
        },
        u'crawler.module': {
            'Meta': {'ordering': "('uzh_url',)", 'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Module'},
            'achievement_test': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'booking_deadline': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'booking_modules'", 'null': 'True', 'to': u"orm['crawler.ModuleDeadline']"}),
            'cancellation_deadline': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cancellation_modules'", 'null': 'True', 'to': u"orm['crawler.ModuleDeadline']"}),
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'db_index': 'True', 'related_name': "'modules'", 'symmetrical': 'False', 'to': u"orm['crawler.Category']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'ects': ('django.db.models.fields.DecimalField', [], {'max_digits': '4', 'decimal_places': '1', 'db_index': 'True'}),
            'frequency': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'to': u"orm['crawler.ModuleFrequency']"}),
            'further_information': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'grading_scale': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'to': u"orm['crawler.ModuleGradingScale']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'modules'", 'symmetrical': 'False', 'through': u"orm['crawler.ModuleKeywordRelation']", 'to': u"orm['crawler.ModuleKeyword']"}),
            'language': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'to': u"orm['crawler.ModuleLanguage']"}),
            'learning_target': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'organizational_units': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.ModuleOrganizational']"}),
            'persons_in_charge': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['crawler.Person']"}),
            'precognition': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'repeatability': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'modules'", 'null': 'True', 'to': u"orm['crawler.ModuleRepeatability']"}),
            'requirements': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'target_audience': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'teaching_materials': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_code': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.moduledeadline': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleDeadline'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.modulefrequency': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleFrequency'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.modulegradingscale': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleGradingScale'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.modulekeyword': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleKeyword'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.modulekeywordrelation': {
            'Meta': {'ordering': "('module', 'keyword')", 'object_name': 'ModuleKeywordRelation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keyword': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.ModuleKeyword']"}),
            'module': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Module']"}),
            'weight': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '5', 'db_index': 'True'})
        },
        u'crawler.modulelanguage': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleLanguage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.moduleorganizational': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleOrganizational'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'db_index': 'True', 'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'crawler.modulerepeatability': {
            'Meta': {'ordering': "('title',)", 'object_name': 'ModuleRepeatability'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255', 'db_index': 'True'})
        },
        u'crawler.person': {
            'Meta': {'ordering': "('firstname', 'lastname')", 'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Person'},
            'firstname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'position': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.room': {
            'Meta': {'unique_together': "(('uzh_id', 'semester'),)", 'object_name': 'Room'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'semester': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['crawler.Semester']"}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'}),
            'uzh_url': ('django.db.models.fields.URLField', [], {'unique': 'True', 'max_length': '200', 'db_index': 'True'})
        },
        u'crawler.semester': {
            'Meta': {'ordering': "('crawl_date',)", 'object_name': 'Semester'},
            'crawl_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 8, 12, 0, 0)', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'semester_end': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'semester_start': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_index': 'True'})
        }
    }

    complete_apps = ['crawler']