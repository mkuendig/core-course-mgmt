from crawler.models import Semester
from django.utils.datetime_safe import datetime


def add_semesters():
    """
    Adds all semesters from fs13 - hs15
    :return:
    """
    fs13 = Semester.create_or_update('FS13')
    fs13.semester_start = datetime(2013, 2, 1)
    fs13.semester_end = datetime(2013, 7, 31)
    fs13.save()

    hs13 = Semester.create_or_update('HS13')
    hs13.semester_start = datetime(2013, 8, 1)
    hs13.semester_end = datetime(2014, 1, 31)
    hs13.save()

    fs14 = Semester.create_or_update('FS14')
    fs14.semester_start = datetime(2014, 2, 1)
    fs14.semester_end = datetime(2014, 7, 31)
    fs14.save()

    hs14 = Semester.create_or_update('HS14')
    hs14.semester_start = datetime(2014, 8, 1)
    hs14.semester_end = datetime(2015, 1, 31)
    hs14.save()

    fs15 = Semester.create_or_update('FS15')
    fs15.semester_start = datetime(2015, 2, 1)
    fs15.semester_end = datetime(2015, 7, 21)
    fs15.save()

    hs15 = Semester.create_or_update('HS15')
    hs15.semester_start = datetime(2015, 8, 1)
    hs15.semester_end = datetime(2016, 1, 31)
    hs15.save()

    fs16 = Semester.create_or_update('FS16')
    fs16.semester_start = datetime(2016, 2, 1)
    fs16.semester_end = datetime(2016, 7, 31)
    fs16.save()

    hs16 = Semester.create_or_update('HS16')
    hs16.semester_start = datetime(2016, 8, 1)
    hs16.semester_end = datetime(2017, 1, 31)
    hs16.save()
