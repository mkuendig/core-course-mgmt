Installation
=========

1. Make sure you have installed Python 2.7.x and virtualenv

2. Check out the dev branch:

        git checkout dev   

3. Create a virtualenv and activate it

        virtualenv ./virtualenv
        source ./virtualenv/bin/activate

4. Install the required python packages:
        
        pip install -r ./requirements/dev.txt

5. Ensure you have installed and running a Postgres Server. Ensure there is a superuser postgres.

6. Create the coursemgmt/settings/dev.py file from the coursemgmt/settings./dev.py.smpl sample

7. 

8. You can now start the django runserver:

        cd {{ project_name }}
        python manage.py runserver
        
9. Visit ```localhost:8000/admin/```

10. Please change the FTP credentials in the file ```/fab_project_settings.conf``` to another. The FTP server is used to store and load db dumps.

        
Crawl Semester
-----------------

You can trigger the crawl process using either the admin GUI or a manage.py command:

    source bin/activate
    cd coursemgmt
    python manage.py crawl_semester HS15
    
Export to Meteor (Frontend)
-----------------

Ensure the meteor project is running.

You can trigger the export process using either the admin GUI or a manage.py command:
    
    source bin/activate
    cd coursemgmt
    python manage.py export_to_meteor
    
If there are any problems check the ```METEOR_EXPORT_URL``` setting in the ```coursemgmt/settings/dev.py``` file.

